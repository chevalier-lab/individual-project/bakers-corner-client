package com.aitekteam.developer.bakerscorner.model

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class FeedbackItem (
    var uid: String? = "",
    var user: User? = User(),
    var description: String? = "",
    var timestamp: Long? = System.currentTimeMillis()
) : Parcelable {
    override fun equals(other: Any?): Boolean {
        return if (other is FeedbackItem) {
            other.uid == uid
        } else false
    }

    override fun hashCode(): Int {
        var result = uid?.hashCode() ?: 0
        result = 31 * result + (user?.hashCode() ?: 0)
        result = 31 * result + (description?.hashCode() ?: 0)
        result = 31 * result + (timestamp?.hashCode() ?: 0)
        return result
    }
}