package com.aitekteam.developer.bakerscorner.ui.profile.requests

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscorner.R
import com.aitekteam.developer.bakerscorner.databinding.ActivityListRequestBinding
import com.aitekteam.developer.bakerscorner.model.ProductRequest
import com.aitekteam.developer.bakerscorner.ui.home.activity.HomeActivity
import com.aitekteam.developer.bakerscorner.ui.request.RequestViewModel
import com.aitekteam.developer.bakerscorner.utils.*
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.item_request.view.*
import org.marproject.reusableadapter.interfaces.AdapterCallback
import org.marproject.reusableadapter.ReusableAdapter
import java.lang.StringBuilder

@Suppress("SpellCheckingInspection")
class ListRequestActivity : AppCompatActivity() {

    // viewmodel & binding
    private lateinit var viewModel: RequestViewModel
    private lateinit var binding: ActivityListRequestBinding

    // utils
    private lateinit var user: FirebaseAuth
    private lateinit var adapter: ReusableAdapter<ProductRequest>
    private lateinit var listRequest: MutableList<ProductRequest>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListRequestBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(this).get(RequestViewModel::class.java)

        // utils init
        user = FirebaseAuth.getInstance()
        listRequest = mutableListOf()

        // set progress bar
        binding.progressbar.visibility = View.VISIBLE

        // init adapter
        adapter = ReusableAdapter(this)

        // init data
        initRequestData()

        // back button
        binding.back.setOnClickListener { finish() }

        // get parcelable bool data from DetailRequestActivity
        val notifCancel = intent.getBooleanExtra("notifCancel", false)
        val notifSuccess = intent.getBooleanExtra("notifSuccess", false)
        if (notifCancel) {
            SnackbarUtils.withoutAction(window.decorView, "Sukses hapus request!",
                ContextCompat.getColor(this, R.color.text_hitam_utama),
                ContextCompat.getColor(this, R.color.white)
            )
        } else if (notifSuccess) {
            SnackbarUtils.withoutAction(window.decorView, "Berhasil update request!",
                ContextCompat.getColor(this, R.color.text_hitam_utama),
                ContextCompat.getColor(this, R.color.white)
            )
        }

        // set content view
        setContentView(binding.root)
    }

    private fun initRequestData() {
        // ambil data request untuk dimasukkan ke dalam adapter
        viewModel.getOrders {
            Log.i("listRequest", "data kosong")
            // hide progress bar
            binding.progressbar.visibility = View.GONE
            binding.isEmpty.visibility = View.VISIBLE
        }
        viewModel.orders.observe({ lifecycle }, {
            // filter data by user uid
            val listOrder = it.filter { op -> op.user?.uid == user.uid }

            // add filtered data to listRequest
            listOrder.map { op ->
                op.productRequests?.map { req ->
                    val data = ProductRequest(req.uid, req.name, req.price, req.qty,
                        req.is_available, req.is_accept, req.description, req.productCategory, op.time_stamp)
                    listRequest.add(data)
                }
            }

            if (listRequest.isNotEmpty()) {
                Log.i("listRequest", "ada data")
                // add data to adapter
                setupAdapter(binding.rvRequest, listRequest.sortedBy { req -> req.is_accept })
            } else {
                Log.i("listRequest", "data kosong")
                // hide progress bar
                binding.progressbar.visibility = View.GONE
                binding.isEmpty.visibility = View.VISIBLE
            }

            // hide progress bar
            binding.progressbar.visibility = View.GONE
        })

        // realtime update
        viewModel.getRealtimeUpdate()
        viewModel.order.observe({ lifecycle }, {
            val requests = it.productRequests!!

            // check for each user
            if (it.user!!.uid == user.uid) {
                requests.map { req ->
                    val data = ProductRequest(req.uid, req.name, req.price, req.qty,
                        req.is_available, req.is_accept, req.description, req.productCategory, it.time_stamp)

                    // append updated data to adapter
                    adapter.updateData(data)
                }
            }
        })
    }

    private fun getSnackbar(view: View, message: String) {
        SnackbarUtils.show(view, message,
            ContextCompat.getColor(this, R.color.colorPrimary),
            ContextCompat.getColor(this, R.color.text_hitam_utama),
            ContextCompat.getColor(this, R.color.white))
    }

    private fun setupAdapter(recyclerView: RecyclerView, items: List<ProductRequest>) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_request)
            .addData(items)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object :
        AdapterCallback<ProductRequest> {
        override fun initComponent(itemView: View, data: ProductRequest, itemIndex: Int) {
            // set utils
            itemView.tv_nama_makanan.text = data.name
            itemView.tv_harga.text = rupiah(data.price!!.toDouble())
            itemView.tv_tanggal.text = convertLongToDateString(data.timestamp!!)
            itemView.tv_qty.text = getString(R.string.show_qty_2, data.qty.toString())

            // set status makanan
            val setStatus = fun (text: String, color: Int) {
                itemView.tv_status.text = StringBuilder().append(text)
                itemView.tv_status.setTextColor(ContextCompat
                    .getColor(this@ListRequestActivity, color))
            }

            when (data.is_accept) {
                0 -> setStatus("Belum diproses", R.color.text_brown)
                1 -> setStatus("Diproses", R.color.text_orange)
                2 -> setStatus("Ditolak", R.color.text_red)
                3 -> setStatus("Sukses", R.color.text_green)
            }
        }

        override fun onItemClicked(itemView: View, data: ProductRequest, itemIndex: Int) {
            when (data.is_accept) {
                0 -> {
                    startActivity(
                        Intent(this@ListRequestActivity, DetailRequestActivity::class.java)
                            .putExtra("request", data)
                    )
                }
                1 -> getSnackbar(itemView, "Pesanan anda sedang di proses")
                2 -> getSnackbar(itemView, "Pesanan anda ditolak")
                3 -> getSnackbar(itemView, "Pesanan anda telah selesai")
            }
        }

    }

}
