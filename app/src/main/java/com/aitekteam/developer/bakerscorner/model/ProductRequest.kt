package com.aitekteam.developer.bakerscorner.model

import android.os.Parcelable
import com.google.firebase.database.Exclude
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Suppress("SpellCheckingInspection")
@JsonClass(generateAdapter = true)
@Parcelize
data class ProductRequest (
    var uid: String? = "",
    var name: String? = "",
    var price: Long? = 0,
    var qty: Int? = 0,
    var is_available: Int? = 0,
    var is_accept: Int? = 0,
    var description: String? = "",
    var productCategory: ProductCategory? = ProductCategory(),
    @get:Exclude
    var timestamp: Long? = 0
) : Parcelable {

    override fun equals(other: Any?): Boolean {
        return if (other is ProductRequest) {
            other.uid == uid
        } else false
    }

    override fun hashCode(): Int {
        var result = uid?.hashCode() ?: 0
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (price?.hashCode() ?: 0)
        result = 31 * result + (qty ?: 0)
        result = 31 * result + (is_available ?: 0)
        result = 31 * result + (is_accept ?: 0)
        result = 31 * result + (description?.hashCode() ?: 0)
        result = 31 * result + (productCategory?.hashCode() ?: 0)
        return result
    }
}