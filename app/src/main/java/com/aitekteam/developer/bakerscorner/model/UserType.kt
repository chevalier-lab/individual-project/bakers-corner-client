package com.aitekteam.developer.bakerscorner.model

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class UserType (
    var uid: String? = "",
    var type: Int = 0
) : Parcelable