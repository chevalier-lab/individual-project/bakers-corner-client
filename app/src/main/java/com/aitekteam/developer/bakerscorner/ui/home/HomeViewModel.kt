package com.aitekteam.developer.bakerscorner.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aitekteam.developer.bakerscorner.model.OrderProduct
import com.aitekteam.developer.bakerscorner.model.Product
import com.aitekteam.developer.bakerscorner.model.ProductCategory
import com.aitekteam.developer.bakerscorner.utils.Constants.NODE_CATEGORY
import com.aitekteam.developer.bakerscorner.utils.Constants.NODE_ORDER
import com.aitekteam.developer.bakerscorner.utils.Constants.NODE_PRODUCT
import com.google.firebase.database.*

@Suppress("SpellCheckingInspection")
class HomeViewModel : ViewModel() {

    // buat database
    private val dbCategories = FirebaseDatabase.getInstance().getReference(NODE_CATEGORY)
    private val dbProducts = FirebaseDatabase.getInstance().getReference(NODE_PRODUCT)
    private val dbOrders = FirebaseDatabase.getInstance().getReference(NODE_ORDER)

    // buat category
    private val _categories = MutableLiveData<List<ProductCategory>>()
    val categories: LiveData<List<ProductCategory>>
        get() = _categories

    // buat products
    private val _products = MutableLiveData<List<Product>>()
    val products: LiveData<List<Product>>
        get() = _products

    // buat product untuk realtime update
    private val _product = MutableLiveData<Product>()
    val product: LiveData<Product>
    get() = _product

    // buat list order
    private val _orders = MutableLiveData<List<OrderProduct>>()
    val orders: LiveData<List<OrderProduct>>
        get() = _orders

    // get categories data
    fun fetchCategories() {
        dbCategories.addListenerForSingleValueEvent(valueEventListener)
    }

    private val valueEventListener = object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) { }

        override fun onDataChange(snapshot: DataSnapshot) {
            if (snapshot.exists()) {
                val categories = mutableListOf<ProductCategory>()
                for (categorySnapshot in snapshot.children) {
                    val category = categorySnapshot.getValue(ProductCategory::class.java)
                    category?.uid = categorySnapshot.key
                    category?.let { categories.add(it) }
                }
                _categories.value = categories
            }
        }
    }

    // get products data
    fun fetchProducts() {
        dbProducts.addListenerForSingleValueEvent(productValueEventListener)
    }

    private val productValueEventListener = object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) { }

        override fun onDataChange(snapshot: DataSnapshot) {
            if (snapshot.exists()) {
                val products = mutableListOf<Product>()
                for (productSnapshot in snapshot.children) {
//                    val product = productSnapshot.getValue(Product::class.java)
                    val product = Product(
                        productSnapshot.child("uid").getValue(String::class.java),
                        productSnapshot.child("name").getValue(String::class.java),
                        productSnapshot.child("price").getValue(Long::class.java),
                        productSnapshot.child("qty").getValue(Int::class.java),
                        productSnapshot.child("_available").getValue(Int::class.java),
                        productSnapshot.child("photos").getValue(String::class.java),
                        productSnapshot.child("search").getValue(String::class.java),
                        productSnapshot.child("description").getValue(String::class.java),
                        productSnapshot.child("productCategory").getValue(ProductCategory::class.java)
                    )
                    product.uid = productSnapshot.key
                    product.let { products.add(it) }
                }
                _products.value = products
            }
        }
    }

    // realtime update
    fun realtimeUpdate() {
        dbProducts.addChildEventListener(getRealtimeUpate)
    }

    private val getRealtimeUpate = object : ChildEventListener {
        override fun onCancelled(p0: DatabaseError) { }

        override fun onChildMoved(p0: DataSnapshot, p1: String?) { }

        override fun onChildRemoved(data: DataSnapshot) { }

        override fun onChildChanged(productSnapshot: DataSnapshot, p1: String?) {
            val product = Product(
                productSnapshot.child("uid").getValue(String::class.java),
                productSnapshot.child("name").getValue(String::class.java),
                productSnapshot.child("price").getValue(Long::class.java),
                productSnapshot.child("qty").getValue(Int::class.java),
                productSnapshot.child("_available").getValue(Int::class.java),
                productSnapshot.child("photos").getValue(String::class.java),
                productSnapshot.child("search").getValue(String::class.java),
                productSnapshot.child("description").getValue(String::class.java),
                productSnapshot.child("productCategory").getValue(ProductCategory::class.java)
            )
            product.uid = productSnapshot.key
            _product.value = product
        }

        override fun onChildAdded(productSnapshot: DataSnapshot, p1: String?) {
            val product = Product(
                productSnapshot.child("uid").getValue(String::class.java),
                productSnapshot.child("name").getValue(String::class.java),
                productSnapshot.child("price").getValue(Long::class.java),
                productSnapshot.child("qty").getValue(Int::class.java),
                productSnapshot.child("_available").getValue(Int::class.java),
                productSnapshot.child("photos").getValue(String::class.java),
                productSnapshot.child("search").getValue(String::class.java),
                productSnapshot.child("description").getValue(String::class.java),
                productSnapshot.child("productCategory").getValue(ProductCategory::class.java)
            )
            product.uid = productSnapshot.key
            _product.value = product
        }

    }

    // get list order for feedback order
    fun checkOrders(userUid: String) {
        dbOrders.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) { }

            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    val orders = mutableListOf<OrderProduct>()

                    for (orderSnapshot in snapshot.children) {
                        val order = orderSnapshot.getValue(OrderProduct::class.java)

                        order?.let {
                            if (it.user?.uid == userUid) {
                                orders.add(it)
                            }
                        }
                    }

                    _orders.value = orders
                }

                dbOrders.removeEventListener(this)
            }

        })
    }

    override fun onCleared() {
        super.onCleared()
        dbCategories.removeEventListener(valueEventListener)
        dbProducts.removeEventListener(productValueEventListener)
        dbProducts.removeEventListener(getRealtimeUpate)
    }
}