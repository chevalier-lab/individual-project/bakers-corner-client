package com.aitekteam.developer.bakerscorner

import android.app.Application
import androidx.work.*
import com.aitekteam.developer.bakerscorner.service.NotificationWorker
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BakersCornerApplication : Application() {

    private val applicationScope = CoroutineScope(Dispatchers.Default)

    override fun onCreate() {
        super.onCreate()
        applicationScope.launch {
            setupWorker()
        }
    }

    private fun setupWorker() {
        // create task
        val task = OneTimeWorkRequest.Builder(NotificationWorker::class.java).build()
        val workManager = WorkManager.getInstance()
        workManager.enqueue(task)
    }
}