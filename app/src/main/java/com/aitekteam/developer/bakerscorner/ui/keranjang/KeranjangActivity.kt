package com.aitekteam.developer.bakerscorner.ui.keranjang

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscorner.R
import com.aitekteam.developer.bakerscorner.database.Keranjang
import com.aitekteam.developer.bakerscorner.database.KeranjangDatabase
import com.aitekteam.developer.bakerscorner.databinding.ActivityKeranjangBinding
import com.aitekteam.developer.bakerscorner.model.OrderProduct
import com.aitekteam.developer.bakerscorner.model.Product
import com.aitekteam.developer.bakerscorner.model.ProductRequest
import com.aitekteam.developer.bakerscorner.ui.checkout.CheckoutActivity
import com.aitekteam.developer.bakerscorner.ui.profile.ProfileViewModel
import com.aitekteam.developer.bakerscorner.utils.*
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.item_keranjang.view.*
import org.marproject.reusableadapter.ReusableAdapter
import org.marproject.reusableadapter.interfaces.AdapterCallback
import java.util.*

@Suppress("SpellCheckingInspection")
class KeranjangActivity : AppCompatActivity() {

    // buat binding dan viewmodel
    private lateinit var binding: ActivityKeranjangBinding
    private lateinit var viewModel: KeranjangViewModel
    private lateinit var profileViewModel: ProfileViewModel

    // buat user dari firebase auth untuk ambil current user
    private lateinit var user: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityKeranjangBinding.inflate(layoutInflater)
        user = FirebaseAuth.getInstance()

        // request viewmodel init
        val application = requireNotNull(this).application
        val dataSource = KeranjangDatabase.getInstance(application).keranjangDao
        val factory = KeranjangViewModel.Factory(dataSource, application)
        viewModel = ViewModelProvider(this, factory).get(KeranjangViewModel::class.java)

        // profile viewmodel init
        profileViewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)

        // init UI
        initUI()

        // set content view
        setContentView(binding.root)
    }

    private fun initUI() {
        // observe data dari profile
        profileViewModel.initData(user.uid!!)

        // observe data request dari room db
        viewModel.keranjang.observe({ lifecycle }, {
            // setup adapeter
            setupAdapter(binding.rvKeranjang, it)
        })

        // proses checkout
        binding.btnProses.setOnClickListener {
            DialogUtils.showDialog(this,
                getString(R.string.perhatian),
                getString(R.string.checkout_disclaimer)) {
                doProcess(it)
            }
        }

        // back button
        binding.back.setOnClickListener { finish() }
    }

    private fun doProcess(view: View) {
        // init utils
        var listRequest = mutableListOf<ProductRequest>()
        var listProduct = mutableListOf<Product>()
        val listType = mutableListOf<Int>()
        var totalHarga: Long = 0

        // observe data request from room db
        viewModel.keranjang.observe({ lifecycle }, {
            listProduct = it.filter { p -> p.type == 0 }.convertToProduct() as MutableList<Product>
            listRequest = it.filter { p -> p.type == 1 }.convertToProductRequest() as MutableList<ProductRequest>
        })

        if (listRequest.isEmpty() && listProduct.isEmpty()) {
            getSnackbar(view, "Keranjang kamu kosong!")
        } else {
            // get total price
            listRequest.map {
                totalHarga += it.price!! * it.qty!!.toLong()
            }

            listProduct.map {
                totalHarga += it.price!! * it.qty!!.toLong()
            }

            // buat data order
            val dataOrder = OrderProduct()
            dataOrder.total_price = totalHarga

            // total qty
            var totalQty = 0
            listRequest.map {
                totalQty += it.qty!!
            }

            listProduct.map {
                totalQty += it.qty!!
            }
            dataOrder.total_qty = totalQty

            // get object user
            profileViewModel.user.observe({ lifecycle }, {
                dataOrder.user = it
            })

            // tipe order
            // jika 0 = product, 1 = request
            if (listProduct.isNotEmpty()) {
                listType.add(0)
            }

            if (listRequest.isNotEmpty()) {
                listType.add(1)
            }
            dataOrder.type = listType

            // data product / request
            dataOrder.products = listProduct
            dataOrder.productRequests = listRequest

            // set id productRequest
            dataOrder.productRequests!!.map { productRequest ->
                productRequest.uid = UUID.randomUUID().toString()
                    .split("-")[4].toUpperCase(Locale.ROOT)
            }

            // kirim data order ke activity checkout
            val intent = Intent(this, CheckoutActivity::class.java)
            intent.putExtra("dataOrder", dataOrder)
            startActivity(intent)
        }
    }

    private fun getSnackbar(view: View, message: String) {
        SnackbarUtils.show(view, message,
            ContextCompat.getColor(this, R.color.colorPrimary),
            ContextCompat.getColor(this, R.color.text_hitam_utama),
            ContextCompat.getColor(this, R.color.white))
    }

    private fun setupAdapter(recyclerView: RecyclerView, items: List<Keranjang>) {
        ReusableAdapter<Keranjang>(this)
            .adapterCallback(adapterCallback)
            .setLayout(R.layout.item_keranjang)
            .addData(items)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object :
        AdapterCallback<Keranjang> {
        override fun initComponent(itemView: View, data: Keranjang, itemIndex: Int) {
            // set utils
            itemView.tv_nama_makanan.text = data.name
            itemView.tv_harga.text = rupiah(data.price.toDouble())
            itemView.tv_qty.text = getString(R.string.show_qty, data.qty.toString())

            // set tipe makanan
            when (data.type) {
                0 -> itemView.tv_type.text = ""
                1 -> itemView.tv_type.text = StringBuilder().append(" (request)")
            }

            // button hapus
            itemView.btn_hapus.setOnClickListener {
                DialogUtils.showDialog(this@KeranjangActivity,
                    getString(R.string.perhatian),
                    getString(R.string.delete_request_disclaimer) ) {
                    viewModel.deleteKeranjang(data.id)
                    getSnackbar(it, "Berhasil hapus makanan!")
                }
            }
        }

        override fun onItemClicked(itemView: View, data: Keranjang, itemIndex: Int) { }
    }
}
