package com.aitekteam.developer.bakerscorner.ui.request

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.aitekteam.developer.bakerscorner.R
import com.aitekteam.developer.bakerscorner.database.Keranjang
import com.aitekteam.developer.bakerscorner.database.KeranjangDatabase
import com.aitekteam.developer.bakerscorner.databinding.FragmentRequestBinding
import com.aitekteam.developer.bakerscorner.model.ProductCategory
import com.aitekteam.developer.bakerscorner.ui.home.HomeFragment
import com.aitekteam.developer.bakerscorner.ui.home.HomeViewModel
import com.aitekteam.developer.bakerscorner.ui.keranjang.KeranjangActivity
import com.aitekteam.developer.bakerscorner.ui.keranjang.KeranjangViewModel
import com.aitekteam.developer.bakerscorner.utils.DialogUtils
import com.aitekteam.developer.bakerscorner.utils.DropdownAdapter
import com.aitekteam.developer.bakerscorner.utils.SnackbarUtils
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_home.*

@Suppress("SpellCheckingInspection")
class RequestFragment : Fragment() {

    // binding
    private var _binding: FragmentRequestBinding? = null
    private val binding get() = _binding!!

    // viewmodel
    private lateinit var viewModel: RequestViewModel
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var requestViewModel: KeranjangViewModel

    // utils
    private lateinit var user: FirebaseAuth
    private lateinit var listCategory: ArrayList<String>
    private lateinit var listCategoryObj: MutableList<ProductCategory>

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentRequestBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this).get(RequestViewModel::class.java)
        user = FirebaseAuth.getInstance()
        listCategory = arrayListOf()
        listCategoryObj = mutableListOf()

        // home viewmodel init
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        homeViewModel.fetchCategories()

        // request viewmodel init
        val application = requireNotNull(this.activity).application
        val dataSource = KeranjangDatabase.getInstance(application).keranjangDao
        val factory = KeranjangViewModel.Factory(dataSource, application)
        requestViewModel = ViewModelProvider(this, factory).get(KeranjangViewModel::class.java)

        // set dropdown category
        DropdownAdapter(requireContext(), binding.category, listCategory) {
            // set data of category
            homeViewModel.categories.observe(viewLifecycleOwner, Observer { category ->
                category.map { p ->
                    listCategory.add(p.category.toString())
                    it.notifyDataSetChanged()
                    listCategoryObj.add(p)
                }
                binding.category.setText(category[0].category, false)
            })
        }.show()

        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnSubmit.setOnClickListener {
            inputCheck()
        }
    }

    private fun inputCheck() {
        when {
            binding.etNamaMakanan.text!!.trim().isEmpty() -> {
                binding.layoutName.error ="Harap isi nama makanan!"
            }
            binding.etHarga.text!!.trim().isEmpty() -> {
                binding.layoutHarga.error ="Harap isi kisaran harga!"
            }
            binding.etQty.text!!.trim().isEmpty() -> {
                binding.layoutQty.error ="Harap isi kuantitas makanan!"
            }
            binding.etKeterangan.text!!.trim().isEmpty() -> {
                binding.layoutKeterangan.error ="Harap isi keterangan makanan!"
            }
            else -> {
                DialogUtils.showDialog(requireContext(),
                    getString(R.string.perhatian),
                    getString(R.string.process_disclaimer)
                ) { doRequest() }
            }
        }
    }

    private fun doRequest() {
        val name = binding.etNamaMakanan.text.toString()
        val description = binding.etKeterangan.text.toString()
        val price = binding.etHarga.text.toString().toLong()
        val qty = binding.etQty.text.toString().toInt()
        val category = binding.category.text.toString()

        // get category object
        val objCategory = ProductCategory()
        listCategoryObj.map {
            if (it.category == category) {
                objCategory.uid = it.uid
                objCategory.icon = it.icon
                objCategory.category = category
            }
        }

        // create data & insert
        val data = Keranjang(0, name, price, qty, 0, 0,
            "", description, objCategory.uid!!, category, objCategory.icon!!, 1)
        requestViewModel.inserDataKeranjang(data)

        // post request / after request
        binding.etNamaMakanan.setText("")
        binding.etKeterangan.setText("")
        binding.etHarga.setText("")
        binding.etQty.setText("")

        // show snackbar
        SnackbarUtils.withAction(
            requireView(),
            "Sukses request makanan!",
            ContextCompat.getColor(requireContext(), R.color.colorPrimary),
            ContextCompat.getColor(requireContext(), R.color.text_hitam_utama),
            ContextCompat.getColor(requireContext(), R.color.white)
        ) {
            startActivity(
                Intent(requireContext(), KeranjangActivity::class.java)
                    .putExtra("kondisiMasuk", "home")
            )
        }
    }

    // back stack
    override fun onResume() {
        super.onResume()
        requireView().isFocusableInTouchMode = true
        requireView().requestFocus()
        requireView().setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                // handle back button's click listener
                requireActivity().bottom_nav.menu.getItem(0).isChecked = true
                requireActivity().supportFragmentManager.beginTransaction().replace(R.id.home_frame, HomeFragment()).commit()
                return@OnKeyListener true
            }
            false
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding == null
    }
}
