package com.aitekteam.developer.bakerscorner.model

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class ProductCategory (
    var uid: String? = "",
    var category: String? = "",
    var icon: String? = ""
) : Parcelable