package com.aitekteam.developer.bakerscorner.ui.profile

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.*
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.aitekteam.developer.bakerscorner.R
import com.aitekteam.developer.bakerscorner.databinding.FragmentProfileBinding
import com.aitekteam.developer.bakerscorner.model.User
import com.aitekteam.developer.bakerscorner.ui.auth.SignInActivity
import com.aitekteam.developer.bakerscorner.ui.home.HomeFragment
import com.aitekteam.developer.bakerscorner.ui.profile.requests.ListRequestActivity
import com.aitekteam.developer.bakerscorner.ui.profile.manage.ManajemenProfileActivity
import com.aitekteam.developer.bakerscorner.ui.profile.orders.RiwayatOrderActivity
import com.aitekteam.developer.bakerscorner.utils.DialogUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_home.*

@Suppress("SpellCheckingInspection")
class ProfileFragment : Fragment() {

    // viewmodel & binding
    private lateinit var viewModel: ProfileViewModel
    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!

    // Utils
    private lateinit var user: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
        user = FirebaseAuth.getInstance()

        // set progress bar
        binding.progressbar.visibility = View.VISIBLE

        // init data profile
        viewModel.initData(user.uid!!)
        viewModel.getRealtimeUpdate {
            val updatedUser = it.getValue(User::class.java)

            if (updatedUser?.uid == user.uid) {
                binding.tvFullname.text = updatedUser?.name
                binding.tvEmail.text = updatedUser?.email

                // image profile
                Glide.with(requireContext())
                    .load(updatedUser?.face)
                    .apply(RequestOptions().circleCrop())
                    .into(binding.imgProfile)
            }
        }

        viewModel.loading.observe(viewLifecycleOwner, Observer {
            if (it == false) {
                binding.progressbar.visibility = View.GONE
            }
        })

        viewModel.user.observe(viewLifecycleOwner, Observer {
            binding.tvFullname.text = it.name
            binding.tvEmail.text = it.email

            // image profile
            Glide.with(requireContext())
                .load(it.face)
                .apply(RequestOptions().circleCrop())
                .into(binding.imgProfile)
        })

        // Inflate the layout for this fragment
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnManajemenProfile.setOnClickListener {
            startActivity(Intent(requireContext(), ManajemenProfileActivity::class.java))
        }

        binding.btnLihatRequest.setOnClickListener {
            startActivity(Intent(requireContext(), ListRequestActivity::class.java))
        }

        binding.btnRiwayat.setOnClickListener {
            startActivity(Intent(requireContext(), RiwayatOrderActivity::class.java))
        }

        binding.btnAbout.setOnClickListener {
            DialogUtils.showDialogNoTitle(requireContext(), R.layout.dialog_about_app)
        }

        binding.btnSignOut.setOnClickListener {
            DialogUtils.showDialog(requireContext(),
                getString(R.string.warning),
                getString(R.string.logout_disclaimer)) {
                logout()
            }
        }
    }

    private fun logout() {
        viewModel.logout(requireContext(), getString(R.string.default_web_client_id))
        viewModel.result.observe(viewLifecycleOwner, Observer {
            if (it == 1) {
                val intent = Intent(requireContext(), SignInActivity::class.java).apply {
                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                }
                startActivity(intent)
            }
        })
    }

    // back stack
    override fun onResume() {
        super.onResume()
        requireView().isFocusableInTouchMode = true
        requireView().requestFocus()
        requireView().setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                // handle back button's click listener
                requireActivity().bottom_nav.menu.getItem(0).isChecked = true
                requireActivity().supportFragmentManager.beginTransaction().replace(R.id.home_frame, HomeFragment()).commit()
                return@OnKeyListener true
            }
            false
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
