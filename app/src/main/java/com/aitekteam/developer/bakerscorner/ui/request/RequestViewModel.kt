package com.aitekteam.developer.bakerscorner.ui.request

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aitekteam.developer.bakerscorner.model.OrderProduct
import com.aitekteam.developer.bakerscorner.utils.Constants.NODE_ORDER
import com.google.android.gms.tasks.Task
import com.google.firebase.database.*

@Suppress("SpellCheckingInspection")
class RequestViewModel : ViewModel() {

    // buat database
    private val dbOrder = FirebaseDatabase.getInstance().getReference(NODE_ORDER)

    // buat list order
    private val _orders = MutableLiveData<List<OrderProduct>>()
    val orders: LiveData<List<OrderProduct>>
        get() = _orders

    // buat order
    private val _order = MutableLiveData<OrderProduct>()
    val order: LiveData<OrderProduct>
        get() = _order

    // fungsi update request
    fun updateData(data: OrderProduct, callback: (Task<Void>) -> Unit) {
        dbOrder.child(data.uid!!).setValue(data).addOnCompleteListener {
            callback(it)
        }
    }

    // fungsi hapus order
    fun deleteDataOrder(data: OrderProduct, callback: (Task<Void>) -> Unit) {
        dbOrder.child(data.uid!!).setValue(null).addOnCompleteListener {
            callback(it)
        }
    }

    // fungsi hapus request jika product ada
    fun deleteRequestConditional(orderUid: String, callback: (Task<Void>) -> Unit) {
        dbOrder.child(orderUid).child("productRequests")
            .setValue(null).addOnCompleteListener {
            callback(it)
        }
    }

    // fungsi get request
    fun getOrders(nullHandler: () -> Unit) {
        dbOrder.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) { }

            override fun onDataChange(data: DataSnapshot) {
                if (data.exists()) {
                    val listOrder = mutableListOf<OrderProduct>()

                    // get data of OrderProduct
                    for (orderSnapshot in data.children) {
                        val order = orderSnapshot.getValue(OrderProduct::class.java)
                        order?.uid = orderSnapshot.key
                        order?.let { listOrder.add(it) }
                    }

                    // add into mutable list
                    _orders.value = listOrder
                } else {
                    nullHandler()
                }

                dbOrder.removeEventListener(this)
            }
        })
    }

    // get realtime update
    fun getRealtimeUpdate() {
        dbOrder.addChildEventListener(realtimeUpdate)
    }

    private val realtimeUpdate = object : ChildEventListener {
        override fun onCancelled(p0: DatabaseError) { }

        override fun onChildMoved(p0: DataSnapshot, p1: String?) { }

        override fun onChildRemoved(data: DataSnapshot) { }

        override fun onChildChanged(data: DataSnapshot, p1: String?) {
            val order = data.getValue(OrderProduct::class.java)
            order?.let { _order.value = it }
        }

        override fun onChildAdded(data: DataSnapshot, p1: String?) {
            val order = data.getValue(OrderProduct::class.java)
            order?.let { _order.value = it }
        }

    }

    override fun onCleared() {
        super.onCleared()
        dbOrder.removeEventListener(realtimeUpdate)
    }

}