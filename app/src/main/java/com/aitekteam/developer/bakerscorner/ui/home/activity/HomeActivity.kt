package com.aitekteam.developer.bakerscorner.ui.home.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.aitekteam.developer.bakerscorner.R
import com.aitekteam.developer.bakerscorner.databinding.ActivityHomeBinding
import com.aitekteam.developer.bakerscorner.service.ReceiverService
import com.aitekteam.developer.bakerscorner.ui.home.HomeFragment
import com.aitekteam.developer.bakerscorner.ui.profile.ProfileFragment
import com.aitekteam.developer.bakerscorner.ui.request.RequestFragment
import com.aitekteam.developer.bakerscorner.ui.search.SearchFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

@Suppress("SpellCheckingInspection")
class HomeActivity : AppCompatActivity() {

    private var currentId: Int? = 0
    var doubleBackToExitPressedOnce: Boolean = false
    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)

        // setup bottomNav
        binding.bottomNav.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        supportFragmentManager.beginTransaction().replace(R.id.home_frame, HomeFragment()).commit()

        // set content view
        setContentView(binding.root)
    }

    // set bottomNavbar
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener {
        val fragmentCheck = fun(fragmentId: Int, fragment: Fragment) {
            if (currentId != fragmentId) {
                supportFragmentManager.beginTransaction().replace(R.id.home_frame, fragment).commit()
            }
        }

        when (it.itemId) {
            R.id.navigation_home -> fragmentCheck(R.id.navigation_home, HomeFragment())
            R.id.navigation_search -> fragmentCheck(R.id.navigation_search, SearchFragment())
            R.id.navigation_request -> fragmentCheck(R.id.navigation_request, RequestFragment())
            R.id.navigation_profile -> fragmentCheck(R.id.navigation_profile, ProfileFragment())
        }

        currentId = it.itemId
        true
    }

    override fun onBackPressed() {
        //Checking for fragment count on backstack
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        } else if (!doubleBackToExitPressedOnce) {
            doubleBackToExitPressedOnce = true
            Toast.makeText(this, "Tekan sekali lagi untuk keluar", Toast.LENGTH_SHORT).show()
            Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
        } else {
            super.onBackPressed()
            return
        }
    }

    override fun onStop() {
        Log.i("BC-MyService", "Kill activity, sending broadcast")
        val broadcastIntent = Intent()
        broadcastIntent.action = "RestartService"
        broadcastIntent.setClass(this, ReceiverService::class.java)
        sendBroadcast(broadcastIntent)
        super.onStop()
    }
}
