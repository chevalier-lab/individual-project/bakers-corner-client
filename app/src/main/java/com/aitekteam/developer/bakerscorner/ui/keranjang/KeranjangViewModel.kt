package com.aitekteam.developer.bakerscorner.ui.keranjang

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.aitekteam.developer.bakerscorner.database.Keranjang
import com.aitekteam.developer.bakerscorner.database.KeranjangDao
import kotlinx.coroutines.*

@Suppress("SpellCheckingInspection")
class KeranjangViewModel(
    val database: KeranjangDao,
    application: Application
) : AndroidViewModel(application) {

    private val viewModelJob = Job()
    private val uiScope =   CoroutineScope(Dispatchers.Main + viewModelJob)
    val keranjang = database.getKeranjang()

    fun inserDataKeranjang(data: Keranjang) {
        uiScope.launch {
            withContext(Dispatchers.IO) {
                database.insertKeranjang(data)
            }
        }
    }

    fun deleteKeranjang(id: Long) {
        uiScope.launch {
            withContext(Dispatchers.IO) {
                database.deleteKeranjang(id)
            }
        }
    }

    fun updateKeranjang(qty: Int, name: String) {
        uiScope.launch {
            withContext(Dispatchers.IO) {
                database.updateKeranjang(qty, name)
            }
        }
    }

    fun clearRequest() {
        uiScope.launch {
            withContext(Dispatchers.IO) {
                database.clearKeranjang()
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    class Factory (
        private val dataSource: KeranjangDao,
        private val application: Application
    ) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {

            if (modelClass.isAssignableFrom(KeranjangViewModel::class.java)) {
                return KeranjangViewModel(
                    dataSource,
                    application
                ) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")

        }

    }
}