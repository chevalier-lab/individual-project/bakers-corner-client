package com.aitekteam.developer.bakerscorner.utils

import android.content.Context
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView

class DropdownAdapter<T>(
    private val context: Context,
    private val dropdown: AutoCompleteTextView,
    private val list: ArrayList<T>,
    private val callback: (ArrayAdapter<T>) -> Unit
) {
    fun show() {
        ArrayAdapter(context, android.R.layout.simple_spinner_item, list).apply {
            this.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            dropdown.setAdapter(this)

            // callback fun
            callback(this)
        }
    }
}