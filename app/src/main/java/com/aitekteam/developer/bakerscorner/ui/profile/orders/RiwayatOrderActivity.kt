package com.aitekteam.developer.bakerscorner.ui.profile.orders

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscorner.R
import com.aitekteam.developer.bakerscorner.databinding.ActivityRiwayatOrderBinding
import com.aitekteam.developer.bakerscorner.model.OrderProduct
import com.aitekteam.developer.bakerscorner.ui.home.activity.HomeActivity
import com.aitekteam.developer.bakerscorner.ui.request.RequestViewModel
import com.aitekteam.developer.bakerscorner.utils.convertLongToDateString
import com.aitekteam.developer.bakerscorner.utils.rupiah
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.item_order.view.*
import org.marproject.reusableadapter.interfaces.AdapterCallback
import org.marproject.reusableadapter.ReusableAdapter
import java.lang.StringBuilder

@Suppress("SpellCheckingInspection")
class RiwayatOrderActivity : AppCompatActivity() {

    // viewmodel & binding
    private lateinit var viewModel: RequestViewModel
    private lateinit var binding: ActivityRiwayatOrderBinding

    // utils
    private lateinit var user: FirebaseAuth
    private lateinit var adapter: ReusableAdapter<OrderProduct>
    private lateinit var listOrder: MutableList<OrderProduct>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRiwayatOrderBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(this).get(RequestViewModel::class.java)

        // utils init
        listOrder = mutableListOf()
        user = FirebaseAuth.getInstance()

        // set progress bar
        binding.progressbar.visibility = View.VISIBLE

        // init adapter
        adapter = ReusableAdapter(this)

        // init order data
        initOrderData()

        // back button
        binding.back.setOnClickListener { finish() }

        // set content view
        setContentView(binding.root)
    }

    private fun initOrderData() {
        // ambil data order untuk dimasukkan ke dalam adapter
        viewModel.getOrders {
            // hide progress bar
            binding.progressbar.visibility = View.GONE
            binding.isEmpty.visibility = View.VISIBLE
        }
        viewModel.orders.observe({ lifecycle }, {
            // filter data by user uid
            val listOrder = it.filter { op -> op.user?.uid == user.uid }

            if (listOrder.isNotEmpty()) {
                // append data to adapter
                setupAdapter(binding.rvOrder,
                    listOrder.sortedByDescending { op -> op.time_stamp }
                        .sortedBy { op -> op.is_accept })
            } else {
                // hide progress bar
                binding.progressbar.visibility = View.GONE
                binding.isEmpty.visibility = View.VISIBLE
            }

            // hide progress bar
            binding.progressbar.visibility = View.GONE
        })

        // realtime update
        viewModel.getRealtimeUpdate()
        viewModel.order.observe({ lifecycle }, {
            if (it.user?.uid == user.uid) {
                // append updated data to adapter
                adapter.updateData(it)
            }
        })
    }

    private fun setupAdapter(recyclerView: RecyclerView, items: List<OrderProduct>) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_order)
            .addData(items)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object :
        AdapterCallback<OrderProduct> {
        override fun initComponent(itemView: View, data: OrderProduct, itemIndex: Int) {
            // init utils
            itemView.tv_harga.text = rupiah(data.total_price!!.toDouble())
            itemView.tv_tanggal.text = convertLongToDateString(data.time_stamp!!)
            itemView.tv_order_code.text = StringBuilder().append("Order #").append(data.order_code)
            itemView.tv_qty.text = getString(R.string.show_qty_2, data.total_qty.toString())

            // tipe order
            if (data.type!!.size == 2) {
                itemView.tv_type.text = StringBuilder().append("Tipe : Product, Request")
            } else if (data.type!![0] == 0) {
                itemView.tv_type.text = StringBuilder().append("Tipe : Product")
            } else {
                itemView.tv_type.text = StringBuilder().append("Tipe : Request")
            }

            // set status
            val setStatus = fun(text: String, color: Int) {
                itemView.tv_status.text = StringBuilder().append(text)
                itemView.tv_status.setTextColor(ContextCompat
                    .getColor(this@RiwayatOrderActivity, color))
            }

            when (data.is_accept) {
                0 -> setStatus("Belum diproses", R.color.text_brown)
                1 -> setStatus("Diproses", R.color.text_orange)
                2 -> setStatus("Ditolak", R.color.text_red)
                3 -> setStatus("Sukses", R.color.text_green)
            }
        }

        override fun onItemClicked(itemView: View, data: OrderProduct, itemIndex: Int) {
            startActivity(
                Intent(this@RiwayatOrderActivity, DetailOrderActivity::class.java)
                    .putExtra("dataOrder", data)
            )
        }

    }
}
