@file:Suppress("SpellCheckingInspection")

package com.aitekteam.developer.bakerscorner.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface KeranjangDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertKeranjang(data: Keranjang)

    @Query("SELECT * FROM keranjang")
    fun getKeranjang(): LiveData<List<Keranjang>>

    @Query("DELETE FROM keranjang WHERE id = :keranjangId")
    fun deleteKeranjang(keranjangId: Long)

    @Query("DELETE FROM keranjang")
    fun clearKeranjang()

    @Query("UPDATE keranjang SET qty = :qty WHERE name = :name")
    fun updateKeranjang(qty: Int, name: String)
}