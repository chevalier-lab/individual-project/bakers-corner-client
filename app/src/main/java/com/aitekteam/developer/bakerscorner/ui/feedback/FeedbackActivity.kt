package com.aitekteam.developer.bakerscorner.ui.feedback

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.aitekteam.developer.bakerscorner.R
import com.aitekteam.developer.bakerscorner.databinding.ActivityFeedbackBinding
import com.aitekteam.developer.bakerscorner.model.FeedbackItem
import com.aitekteam.developer.bakerscorner.model.Product
import com.aitekteam.developer.bakerscorner.model.User
import com.aitekteam.developer.bakerscorner.ui.home.activity.DetailProdukActivity
import com.aitekteam.developer.bakerscorner.utils.convertLongToDateString
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.item_feedback.view.*
import org.marproject.reusableadapter.ReusableAdapter
import org.marproject.reusableadapter.interfaces.AdapterCallback

@Suppress("SpellCheckingInspection")
class FeedbackActivity : AppCompatActivity() {

    // binding & viewmodel
    private lateinit var binding: ActivityFeedbackBinding
    private lateinit var viewModel: FeedbackViewModel

    // adapter
    private lateinit var adapter: ReusableAdapter<FeedbackItem>

    // utils
    private lateinit var user: FirebaseAuth
    private lateinit var dataUser: User
    private lateinit var feedbacks: MutableList<FeedbackItem>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFeedbackBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(this).get(FeedbackViewModel::class.java)

        // ini utils
        user = FirebaseAuth.getInstance()
        dataUser = User()
        feedbacks = mutableListOf()
        binding.fab.visibility = View.GONE

        // get parcelable data
        val dataProduk = intent.getParcelableExtra<Product>("dataProduk")!!

        // init adapter
        adapter = ReusableAdapter(this)

        // setup adapter
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_feedback)
            .isVerticalView()
            .build(binding.rvFeedback)

        // init ui
        initUI(dataProduk)

        // set content view
        setContentView(binding.root)
    }

    private fun initUI(dataProduk: Product) {
        // get user
        viewModel.getUser(user.uid!!)
        viewModel.user.observe({ lifecycle }, {
            dataUser = it
        })

        // init feedbacks
        viewModel.fetchFeedbacks(dataProduk.uid!!)
        viewModel.feedbacks.observe({ lifecycle }, {
            if (it.isEmpty()) {
                binding.tvKosong.visibility = View.VISIBLE
            } else {
                binding.tvKosong.visibility = View.GONE
            }

            // append data to feedbacks for realtime sort
            feedbacks = it.toMutableList()
//            it.map { feedback ->
//                feedbacks.add(feedback)
//            }


            adapter.addData(it)
        })

        // realtime update
        viewModel.getRealtimeUpdate(dataProduk.uid!!)
        viewModel.feedback.observe({ lifecycle }, {

            // get realtime image picture of user
            viewModel.findUser(it.user!!.uid!!) { ds ->
                it.user = ds.getValue(User::class.java)

                // update feedbacks
                if (!feedbacks.contains(it)) {
                    feedbacks.add(it)
                } else {
                    val index = feedbacks.indexOf(it)
                    feedbacks[index] = it
                }

                // realtime sort by timestamp
                adapter.addData(feedbacks.sortedByDescending { sort -> sort.timestamp })
            }

            // cek data null / tidak
            if (it != null) {
                binding.tvKosong.visibility = View.GONE
            }
        })

        // init image dan nama makanan
        Glide.with(this)
            .load(dataProduk.photos)
            .into(binding.imageProduct)
        binding.tvNamaMakanan.text = dataProduk.name

        // loading
        viewModel.loading.observe({ lifecycle }, {
            if (it == false) {
                binding.progressbar.visibility = View.GONE
                binding.fab.visibility = View.VISIBLE
            }
        })

        // back button
        binding.back.setOnClickListener { finish() }

        // add feedback
        binding.fab.setOnClickListener {
            AddFeedbackDialogFragment(dataProduk, dataUser).show(supportFragmentManager, "")
        }
    }

    private val adapterCallback = object : AdapterCallback<FeedbackItem> {
        override fun initComponent(itemView: View, data: FeedbackItem, itemIndex: Int) {
            // set utils
            itemView.tv_username.text = data.user?.name
            itemView.tv_feedback.text = data.description
            itemView.tv_tanggal.text = convertLongToDateString(data.timestamp!!)

            //set gambar
            Glide.with(this@FeedbackActivity)
                .load(data.user!!.face)
                .apply(RequestOptions().circleCrop())
                .into(itemView.image_user)
        }

        override fun onItemClicked(itemView: View, data: FeedbackItem, itemIndex: Int) { }
    }
}