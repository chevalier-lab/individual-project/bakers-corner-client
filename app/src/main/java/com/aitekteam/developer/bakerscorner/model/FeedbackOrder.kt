package com.aitekteam.developer.bakerscorner.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FeedbackOrder (
    var uid: String? = "",
    var orderProduct: OrderProduct? = OrderProduct(),
    var description: String? = ""
) : Parcelable