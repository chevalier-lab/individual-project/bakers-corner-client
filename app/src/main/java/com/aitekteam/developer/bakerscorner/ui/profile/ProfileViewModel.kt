package com.aitekteam.developer.bakerscorner.ui.profile

import android.app.ProgressDialog
import android.content.Context
import android.graphics.Bitmap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aitekteam.developer.bakerscorner.model.User
import com.aitekteam.developer.bakerscorner.utils.Constants.NODE_USERS
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import java.io.ByteArrayOutputStream
import java.lang.Exception

@Suppress("SpellCheckingInspection")
class ProfileViewModel : ViewModel() {

    // buat current user
    private lateinit var mGoogleSignInClient: GoogleSignInClient

    // buat database
    private val dbUsers = FirebaseDatabase.getInstance().getReference(NODE_USERS)
    private val storage = FirebaseStorage.getInstance()

    // buat result
    val _result = MutableLiveData<Int>()
    val result: LiveData<Int>
        get() = _result

    // buat user
    private val _user = MutableLiveData<User>()
    val user: LiveData<User>
        get() = _user

    // buat loading
    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    init {
        _loading.value = true
    }

    // fungsi fetch data
    fun initData(uid: String) {
        dbUsers.child(uid).addListenerForSingleValueEvent(initData)
    }

    private val initData = object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) { }

        override fun onDataChange(data: DataSnapshot) {
            if (data.exists()) {
                val key = FirebaseAuth.getInstance()
                val user = data.getValue(User::class.java)
                user!!.uid = key.uid
                _user.value = user
                _loading.value = false
            }
        }
    }

    // fungsi edit profile
    fun updateProfile(user: User) {
        dbUsers.child(user.uid!!).setValue(user).addOnCompleteListener {
            if (it.isSuccessful) {
                _result.value = 2
            } else {
                _result.value = 3
            }
        }
    }

    // fungsi realtime update
    fun getRealtimeUpdate(realtimeChange: (DataSnapshot) -> Unit) {
        dbUsers.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(p0: DatabaseError) { }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) { }

            override fun onChildChanged(data: DataSnapshot, p1: String?) {
                if (data.exists()) {
//                    Log.i("testingUser", data.value.toString())
//                    val key = FirebaseAuth.getInstance()
//                    val user = data.getValue(User::class.java)
//                    user!!.uid = key.uid
//                    _user.value = user
//                    _loading.value = false
                    realtimeChange(data)
                    dbUsers.removeEventListener(this)
                }
            }

            override fun onChildAdded(data: DataSnapshot, p1: String?) { }

            override fun onChildRemoved(data: DataSnapshot) { }
        })
    }

    // upload to storage
    fun uploadToStorage(
        userId: String,
        bitmap: Bitmap?,
        progressDialog: ProgressDialog,
        exceptionListener: (Exception) -> Unit) {
        if (bitmap != null) {
            // Defining the child of storageReference
            val ref = storage.reference.child("faces/photo_" + user.value?.uid)

            progressDialog.apply {
                this.setTitle("Mohon tunggu ...")
                this.setCancelable(false)
                this.setCanceledOnTouchOutside(false)
            }.show()

            // or failure of image
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val data: ByteArray = baos.toByteArray()
            ref.putBytes(data)
                .addOnSuccessListener {
                    // Image uploaded successfully
                    progressDialog.dismiss()
                    ref.downloadUrl.addOnSuccessListener {
                        if (it != null) {
                            dbUsers.child(userId).child("face").setValue(it.toString())
                        }
                    }
                }
                .addOnFailureListener {
                    progressDialog.dismiss()
                    exceptionListener(it)
                }
                .addOnProgressListener {
                    val progress = (100.0 * it.bytesTransferred / it.totalByteCount)
                    progressDialog.setMessage(String.format("Uploaded %.2f", progress) + "%")
                }
        }
    }

    // fungsi logout
    fun logout(context: Context, webClientId: String) {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(webClientId)
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(context, gso)
        FirebaseAuth.getInstance().signOut()
        mGoogleSignInClient.signOut()

        _result.value = 1
    }

    override fun onCleared() {
        super.onCleared()
        dbUsers.removeEventListener(initData)
    }

}