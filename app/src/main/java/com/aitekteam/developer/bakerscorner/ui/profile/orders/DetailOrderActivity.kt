package com.aitekteam.developer.bakerscorner.ui.profile.orders

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.aitekteam.developer.bakerscorner.R
import com.aitekteam.developer.bakerscorner.database.Keranjang
import com.aitekteam.developer.bakerscorner.databinding.ActivityDetailOrderBinding
import com.aitekteam.developer.bakerscorner.model.OrderProduct
import com.aitekteam.developer.bakerscorner.utils.convertProductToKeranjang
import com.aitekteam.developer.bakerscorner.utils.convertRequestToKeranjang
import com.aitekteam.developer.bakerscorner.utils.rupiah
import kotlinx.android.synthetic.main.item_checkout.view.*
import org.marproject.reusableadapter.ReusableAdapter
import org.marproject.reusableadapter.interfaces.AdapterCallback
import java.lang.StringBuilder

@Suppress("SpellCheckingInspection")
class DetailOrderActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailOrderBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailOrderBinding.inflate(layoutInflater)

        // get parcelable data
        val orderProduct = intent.getParcelableExtra<OrderProduct>("dataOrder")!!

        // init ui
        initUI(orderProduct)

        // set content view
        setContentView(binding.root)
    }

    private fun initUI(orderProduct: OrderProduct) {
        // set title
        binding.tvTitle.text = StringBuilder().append("Order #", orderProduct.order_code)
        binding.tvTotal.text = rupiah(orderProduct.total_price!!.toDouble())

        val listKeranjang = mutableListOf<Keranjang>()

        // add request to list data
        orderProduct.productRequests!!.map {
            listKeranjang.add(it.convertRequestToKeranjang())
        }

        // add product to list data
        orderProduct.products!!.map {
            listKeranjang.add(it.convertProductToKeranjang())
        }

        // setup adapter
        ReusableAdapter<Keranjang>(this)
            .adapterCallback(adapterCallback)
            .setLayout(R.layout.item_checkout)
            .addData(listKeranjang)
            .isVerticalView()
            .build(binding.rvOrders)

        // back button
        binding.back.setOnClickListener { finish() }
    }

    private val adapterCallback = object : AdapterCallback<Keranjang> {
        override fun initComponent(itemView: View, data: Keranjang, itemIndex: Int) {
            // set utils
            itemView.tv_nama_makanan.text = data.name
            itemView.tv_qty.text = getString(R.string.show_qty_2, data.qty.toString())

            // set type
            if (data.type == 0) {
                itemView.tv_tipe.text = getString(R.string.show_tipe, "Product")
            } else {
                itemView.tv_tipe.text = getString(R.string.show_tipe, "Request")
                itemView.tv_status.visibility = View.VISIBLE

                // set status makanan
                val setStatus = fun(text: String, color: Int) {
                    itemView.tv_status.text = StringBuilder().append(text)
                    itemView.tv_status.setTextColor(
                        ContextCompat
                            .getColor(this@DetailOrderActivity, color))
                }

                when (data.is_accept) {
                    0 -> setStatus("Belum diproses", R.color.text_brown)
                    1 -> setStatus("Diproses", R.color.text_orange)
                    2 -> setStatus("Ditolak", R.color.text_red)
                    3 -> setStatus("Sukses", R.color.text_green)
                }
            }

            // set total harga
            itemView.tv_harga_total.text = rupiah(data.price.toDouble() * data.qty.toDouble())
        }

        override fun onItemClicked(itemView: View, data: Keranjang, itemIndex: Int) { }
    }
}
