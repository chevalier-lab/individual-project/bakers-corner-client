package com.aitekteam.developer.bakerscorner.model

data class FeedbackProduct(
    var uid: String? = "",
    var description: String? = "",
    var user: User? = User(),
    var product: Product? = Product()
) {
    override fun equals(other: Any?): Boolean {
        return if (other is Product) {
            other.uid == uid
        } else false
    }

    override fun hashCode(): Int {
        var result = uid?.hashCode() ?: 0
        result = 31 * result + (description?.hashCode() ?: 0)
        result = 31 * result + (user?.hashCode() ?: 0)
        result = 31 * result + (product?.hashCode() ?: 0)
        return result
    }
}