@file:Suppress("SpellCheckingInspection")

package com.aitekteam.developer.bakerscorner.utils

import com.aitekteam.developer.bakerscorner.database.Keranjang
import com.aitekteam.developer.bakerscorner.model.Product
import com.aitekteam.developer.bakerscorner.model.ProductCategory
import com.aitekteam.developer.bakerscorner.model.ProductRequest
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

fun convertLongToDateString(systemTime: Long): String {
    return SimpleDateFormat("EEEE, dd MMMM yyyy", Locale("in", "ID"))
        .format(systemTime).toString()
}

fun rupiah(uang: Double): String {
    val localeID =  Locale("in", "ID")
    val numberFormat = NumberFormat.getCurrencyInstance(localeID)
    return numberFormat.format(uang).toString()
}

fun List<Keranjang>.convertToProductRequest(): List<ProductRequest> {
    return map {
        ProductRequest(
            uid = it.requestUid,
            name = it.name,
            price = it.price,
            qty = it.qty,
            is_available = it.is_available,
            is_accept = it.is_accept,
            description = it.description,
            productCategory = ProductCategory(it.categoryId, it.productCategory, it.icon)
        )
    }
}

fun List<Keranjang>.convertToProduct(): List<Product> {
    return map {
        Product(
            uid = it.productUid,
            name = it.name,
            price = it.price,
            qty = it.qty,
            is_available = it.is_available,
            photos = it.photos,
            description = it.description,
            productCategory = ProductCategory(it.categoryId, it.productCategory, it.icon)
        )
    }
}

fun ProductRequest.convertRequestToKeranjang(): Keranjang {
    return Keranjang(
            requestUid = uid!!,
            name = name!!,
            price = price!!,
            qty = qty!!,
            type = 1,
            is_available = is_available!!,
            is_accept = is_accept!!,
            description = description!!,
            categoryId = productCategory!!.uid!!,
            productCategory = productCategory!!.category!!,
            icon = productCategory!!.icon!!
        )
}

fun Product.convertProductToKeranjang(): Keranjang {
    return Keranjang(
            productUid = uid!!,
            name = name!!,
            price = price!!,
            qty = qty!!,
            is_available = is_available!!,
            photos = photos!!,
            description = description!!,
            categoryId = productCategory!!.uid!!,
            productCategory = productCategory!!.category!!,
            icon = productCategory!!.icon!!
        )
}

//fun List<ProductRequest>.convertRequestToKeranjang(): List<Keranjang> {
//    return map {
//        Keranjang(
//            requestUid = it.uid!!,
//            name = it.name!!,
//            price = it.price!!,
//            qty = it.qty!!,
//            type = 1,
//            is_available = it.is_available!!,
//            is_accept = it.is_accept!!,
//            description = it.description!!,
//            categoryId = it.productCategory!!.uid!!,
//            productCategory = it.productCategory!!.category!!,
//            icon = it.productCategory!!.icon!!
//        )
//    }
//}

//fun List<Product>.convertProductToKeranjang(): List<Keranjang> {
//    return map {
//        Keranjang(
//            productUid = it.uid!!,
//            name = it.name!!,
//            price = it.price!!,
//            qty = it.qty!!,
//            is_available = it.is_available!!,
//            photos = it.photos!!,
//            description = it.description!!,
//            categoryId = it.productCategory!!.uid!!,
//            productCategory = it.productCategory!!.category!!,
//            icon = it.productCategory!!.icon!!
//        )
//    }
//}