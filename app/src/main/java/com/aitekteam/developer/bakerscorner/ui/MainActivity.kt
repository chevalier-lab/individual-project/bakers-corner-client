package com.aitekteam.developer.bakerscorner.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.aitekteam.developer.bakerscorner.databinding.ActivityMainBinding
import com.aitekteam.developer.bakerscorner.ui.auth.SignInActivity


class MainActivity : AppCompatActivity() {

    private val loadingTime = 3000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(ActivityMainBinding.inflate(layoutInflater).root)

        Handler().postDelayed({
            val intent = Intent(this, SignInActivity::class.java)
            startActivity(intent)
            finish()
        }, loadingTime.toLong())
    }

}
