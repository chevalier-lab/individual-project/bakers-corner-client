package com.aitekteam.developer.bakerscorner.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import com.aitekteam.developer.bakerscorner.R
import com.aitekteam.developer.bakerscorner.model.OrderProduct
import com.aitekteam.developer.bakerscorner.model.Product
import com.aitekteam.developer.bakerscorner.ui.home.activity.DetailProdukActivity
import com.aitekteam.developer.bakerscorner.ui.profile.orders.DetailOrderActivity
import com.google.firebase.auth.FirebaseAuth
import com.squareup.moshi.Moshi
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended
import org.eclipse.paho.client.mqttv3.MqttMessage

@Suppress("SpellCheckingInspection")
class NotificationService : Service() {

    private var mqtt: MqttService? = null
    private val TAG = "BC-MyService"
    private val productChannelId = "product"
    private val orderChannelId = "order"
    private val channelName = "Notification Service"
    private lateinit var user: FirebaseAuth

    override fun onCreate() {
        super.onCreate()

        // init user
        user = FirebaseAuth.getInstance()

        // create notification
        val manager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel("foreground", "Foreground Service (hide it)", NotificationManager.IMPORTANCE_DEFAULT)
            manager.createNotificationChannel(channel)
        }

        // create builder
        val notificationBuilder = NotificationCompat.Builder(applicationContext, "foreground")
            .setContentText("Aplikasi sedang berjalan, anda bisa menyembunyikan ini di pengaturan")
            .setSmallIcon(R.drawable.logo_bakers)
            .setAutoCancel(true)
            .setNotificationSilent()
            .build()

        startForeground(3, notificationBuilder)
    }

    private fun showMessage(message: String) {
        Log.i(TAG, message)
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)

        mqttService()
        showMessage("Service started")

        return START_STICKY
    }

    private fun mqttService() {
        mqtt = MqttService(applicationContext)
        mqtt!!.setCallback(object : MqttCallbackExtended {
            override fun messageArrived(topic: String?, message: MqttMessage?) {
                showMessage(message.toString())

                val moshi = Moshi.Builder().build()
                val splitWord = topic?.split("/")?.toTypedArray()
                when(splitWord?.get(2)) {
                    "Products" -> {
                        val parseData = moshi.adapter(Product::class.java)
                        val data = parseData.fromJson(message.toString())!!
                        displayProductNotification(data.description!!, data)
                    }
                    "OrderProcess" -> {
                        val parseData = moshi.adapter(OrderProduct::class.java)
                        val data = parseData.fromJson(message.toString())!!
                        if (data.user?.uid == user.uid) {
                            displayOrderNotification("Pesanan kamu dengan order #${data.order_code} lagi diproses nihh", data)
                        }
                    }
                    "OrderCancel" -> {
                        val parseData = moshi.adapter(OrderProduct::class.java)
                        val data = parseData.fromJson(message.toString())!!
                        if (data.user?.uid == user.uid) {
                            displayOrderNotification("Pesanan kamu dengan order #${data.order_code} ditolak", data)
                        }
                    }
                    "OrderSuccess" -> {
                        val parseData = moshi.adapter(OrderProduct::class.java)
                        val data = parseData.fromJson(message.toString())!!
                        if (data.user?.uid == user.uid) {
                            displayOrderNotification("Pesanan kamu dengan order #${data.order_code} selesai!", data)
                        }
                    }
                }
            }

            override fun connectComplete(reconnect: Boolean, serverURI: String?) { }
            override fun connectionLost(cause: Throwable?) { }
            override fun deliveryComplete(token: IMqttDeliveryToken?) { }

        })
    }

    private fun displayProductNotification(content: String, dataProduct: Product) {
        val manager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(productChannelId, channelName, NotificationManager.IMPORTANCE_DEFAULT)
            manager.createNotificationChannel(channel)
        }

        // pending intent
        val intent = Intent(applicationContext, DetailProdukActivity::class.java).apply {
            this.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            this.putExtra("dataProduct", dataProduct)
        }
        val pendingIntent = PendingIntent.getActivity(applicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        // create builder
        val notificationBuilder = NotificationCompat.Builder(applicationContext, productChannelId)
            .setContentTitle("Ada produk baru nihh\uD83D\uDE0B")
            .setContentText(content)
            .setSmallIcon(R.drawable.logo_bakers)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .build()

        manager.notify(1,notificationBuilder)
    }

    private fun displayOrderNotification(content: String, dataOrderProduct: OrderProduct) {
        val manager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(orderChannelId, channelName, NotificationManager.IMPORTANCE_DEFAULT)
            manager.createNotificationChannel(channel)
        }

        // pending intent
        val intent = Intent(applicationContext, DetailOrderActivity::class.java).apply {
            this.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            this.putExtra("dataOrder", dataOrderProduct)
        }
        val pendingIntent = PendingIntent.getActivity(applicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        // create builder
        val notificationBuilder = NotificationCompat.Builder(applicationContext, orderChannelId)
            .setContentTitle("Ada kabar baru nih!")
            .setContentText(content)
            .setSmallIcon(R.drawable.logo_bakers)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .build()

        manager.notify(2,notificationBuilder)
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        showMessage("Task ended, send broadcast")
        val broadcastIntent = Intent()
        broadcastIntent.action = "StartService"
        broadcastIntent.setClass(this, ReceiverService::class.java)
        sendBroadcast(broadcastIntent)
        super.onTaskRemoved(rootIntent)
    }
}