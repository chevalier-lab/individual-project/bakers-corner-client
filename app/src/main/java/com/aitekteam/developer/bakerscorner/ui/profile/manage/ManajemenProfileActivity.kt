package com.aitekteam.developer.bakerscorner.ui.profile.manage

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.aitekteam.developer.bakerscorner.BuildConfig
import com.aitekteam.developer.bakerscorner.R
import com.aitekteam.developer.bakerscorner.databinding.ActivityManajemenProfileBinding
import com.aitekteam.developer.bakerscorner.model.User
import com.aitekteam.developer.bakerscorner.model.UserType
import com.aitekteam.developer.bakerscorner.ui.profile.ProfileViewModel
import com.aitekteam.developer.bakerscorner.utils.DialogUtils
import com.aitekteam.developer.bakerscorner.utils.SnackbarUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.auth.FirebaseAuth
import java.io.FileNotFoundException
import java.io.InputStream

@Suppress("SpellCheckingInspection")
class ManajemenProfileActivity : AppCompatActivity() {

    // viewmodel & binding
    private lateinit var viewModel: ProfileViewModel
    private lateinit var binding: ActivityManajemenProfileBinding

    // Utils
    private var photo = ""
    private val SELECT_PHOTO = 100
    private lateinit var bitmap: Bitmap
    private lateinit var user: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityManajemenProfileBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)

        // Setup firebase utils
        user = FirebaseAuth.getInstance()

        // set progress bar
        binding.progressbar.visibility = View.VISIBLE

        // init data profile
        viewModel.initData(user.uid!!)
        viewModel.getRealtimeUpdate {
            val updatedUser = it.getValue(User::class.java)

            if (updatedUser?.uid == user.uid) {
                photo = updatedUser?.face.toString()
                binding.etNama.setText(updatedUser?.name)
                binding.etEmail.setText(updatedUser?.email)
                binding.etPhone.setText(updatedUser?.phone_number)
                binding.etAddress.setText(updatedUser?.address)

                // image profile
                Glide.with(this)
                    .load(updatedUser?.face)
                    .apply(RequestOptions().circleCrop())
                    .into(binding.imgProfile)
            }
        }

        // init data
        viewModel.user.observe({ lifecycle }, {
            photo = it.face.toString()
            binding.etNama.setText(it.name)
            binding.etEmail.setText(it.email)
            binding.etPhone.setText(it.phone_number)
            binding.etAddress.setText(it.address)

            // image profile
            Glide.with(this)
                .load(it.face)
                .apply(RequestOptions().circleCrop())
                .into(binding.imgProfile)
        })

        viewModel.loading.observe({ lifecycle }, {
            if (it == false) {
                binding.progressbar.visibility = View.GONE
            }
        })

        // update button
        binding.btnUpdate.setOnClickListener {
            DialogUtils.showDialog(this,
                getString(R.string.perhatian),
                getString(R.string.profile_disclaimer)
            ) {  inputCheck(it) }
        }

        // choose image
        binding.btnChooseImg.setOnClickListener { openCamera() }

        // back
        binding.btnBack.setOnClickListener { finish() }

        // set content view
        setContentView(binding.root)
    }

    private fun openCamera() {
        val intent = Intent(Intent.ACTION_PICK).apply {
            this.type = "image/*"
        }
        startActivityForResult(intent, SELECT_PHOTO)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == SELECT_PHOTO) {
            if (resultCode == Activity.RESULT_OK) {
                val selectImage: Uri? = data?.data
                var inputStream: InputStream? = null
                try {
                    if (BuildConfig.DEBUG && selectImage == null) {
                        error("Assertion failed")
                    }
                    inputStream = selectImage?.let { contentResolver.openInputStream(it) }
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                }

                bitmap = BitmapFactory.decodeStream(inputStream)
                binding.imgProfile.setImageBitmap(bitmap)
            }
        }
    }

    private fun inputCheck(view: View) {
        when {
            binding.etNama.text!!.trim().isEmpty() -> {
                binding.layoutName.error = "Nama wajib diisi!"
            }
            binding.etEmail.text!!.trim().isEmpty() -> {
                binding.layoutEmail.error = "Email wajib diisi!"
            }
            binding.etPhone.text!!.trim().isEmpty() -> {
                binding.layoutPhone.error = "Nomor HP wajib diisi!"
            }
            binding.etAddress.text!!.trim().isEmpty() -> {
                binding.layoutAddress.error = "Alamat wajib diisi!"
            }
            else -> updateProfile(view)
        }
    }

    private fun updateProfile(view: View) {
        val nama = binding.etNama.text.toString()
        val email = binding.etEmail.text.toString()
        val phone = binding.etPhone.text.toString()
        val address = binding.etAddress.text.toString()

        // upload image
        if (this::bitmap.isInitialized) {
            viewModel.uploadToStorage(user.uid!!, bitmap, ProgressDialog(this)) {
                Toast.makeText(this,
                    "Failed : " + it.message, Toast.LENGTH_SHORT).show()
            }
        }

        // do update
        val data = User(user.uid, email, nama, phone, address, photo, UserType(user.uid))
        viewModel.updateProfile(data)

        viewModel.result.observe({ lifecycle }, {
            if (it == 2) {
                postUpdate()
                viewModel._result.value = 0
                getSnackbar(view, "Sukses update profile!")
            } else if (it == 3) {
                postUpdate()
                viewModel._result.value = 0
                getSnackbar(view, "Gagal update profile:(")
            }
        })
    }

    private fun postUpdate() {
        binding.layoutName.error = null
        binding.layoutEmail.error = null
        binding.layoutPhone.error = null
        binding.layoutAddress.error = null
    }

    private fun getSnackbar(view: View, message: String) {
        SnackbarUtils.show(view, message,
            ContextCompat.getColor(this, R.color.colorPrimary),
            ContextCompat.getColor(this, R.color.text_hitam_utama),
            ContextCompat.getColor(this, R.color.white)
        )
    }
}
