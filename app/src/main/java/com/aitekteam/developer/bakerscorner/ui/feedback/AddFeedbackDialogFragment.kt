package com.aitekteam.developer.bakerscorner.ui.feedback

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.aitekteam.developer.bakerscorner.databinding.DialogFragmentAddFeedbackBinding
import com.aitekteam.developer.bakerscorner.model.FeedbackItem
import com.aitekteam.developer.bakerscorner.model.FeedbackProduct
import com.aitekteam.developer.bakerscorner.model.Product
import com.aitekteam.developer.bakerscorner.model.User
import java.util.*

@Suppress("SpellCheckingInspection")
class AddFeedbackDialogFragment(
    private val dataProduct: Product,
    private val user: User
) : DialogFragment() {

    // binding & viewmodel
    private var _binding: DialogFragmentAddFeedbackBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: FeedbackViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DialogFragmentAddFeedbackBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this).get(FeedbackViewModel::class.java)
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, android.R.style.Theme_Material_Light_Dialog_MinWidth)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.btnAddFeedback.setOnClickListener {
            val feedback = binding.etFeedback.text.toString().trim()

            if (feedback.isEmpty()) {
                binding.layoutName.error = "Harap isi feedback!"
                return@setOnClickListener
            }

            // add to feedbackProduct
            val feedbackProduct = FeedbackProduct("", feedback, user, dataProduct)

            // add to product
            val uidFeedback = UUID.randomUUID().toString()
                .split("-")[4].toUpperCase(Locale.ROOT)
            val feedbackItem = FeedbackItem(uidFeedback, user, feedback)

            viewModel.addFeedback(feedbackProduct, dataProduct.uid!!, feedbackItem) {
                if (it.isSuccessful) {
                    dismiss()
                } else {
                    dismiss()
                }
            }
        }
    }
}