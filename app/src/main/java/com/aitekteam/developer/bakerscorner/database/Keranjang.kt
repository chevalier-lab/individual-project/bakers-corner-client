package com.aitekteam.developer.bakerscorner.database

import androidx.room.Entity
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey

@Suppress("SpellCheckingInspection")
@Entity(tableName = "keranjang")
data class Keranjang (
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0L,
    var name: String = "",
    var price: Long = 0,
    var qty: Int = 0,
    var is_available: Int = 0,
    var is_accept: Int = 0,
    var photos: String = "",
    var description: String = "",
    var categoryId: String = "",
    var productCategory: String = "",
    var icon: String = "",
    var type: Int = 0,
    var productUid: String = "",
    var requestUid: String = ""
)
