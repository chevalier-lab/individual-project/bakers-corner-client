package com.aitekteam.developer.bakerscorner.utils

object Constants {
    const val NODE_USERS = "users"
    const val NODE_ORDER = "orders"
    const val NODE_PRODUCT = "products"
    const val NODE_FEEDBACK = "feedbacks"
    const val NODE_CATEGORY = "categories"
    const val NODE_FEEDBACK_ITEM = "feedbackItems"
    const val NODE_FEEDBACK_PRODUCT = "feedbackProducts"
    const val NODE_FEEDBACK_ORDER = "feedbackOrders"
}