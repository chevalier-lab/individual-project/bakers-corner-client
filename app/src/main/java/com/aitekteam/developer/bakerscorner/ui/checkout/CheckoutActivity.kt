package com.aitekteam.developer.bakerscorner.ui.checkout

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscorner.R
import com.aitekteam.developer.bakerscorner.database.Keranjang
import com.aitekteam.developer.bakerscorner.database.KeranjangDatabase
import com.aitekteam.developer.bakerscorner.databinding.ActivityCheckoutBinding
import com.aitekteam.developer.bakerscorner.model.OrderProduct
import com.aitekteam.developer.bakerscorner.service.MqttService
import com.aitekteam.developer.bakerscorner.ui.home.activity.HomeActivity
import com.aitekteam.developer.bakerscorner.ui.keranjang.KeranjangViewModel
import com.aitekteam.developer.bakerscorner.ui.profile.orders.DetailOrderActivity
import com.aitekteam.developer.bakerscorner.utils.*
import com.squareup.moshi.Moshi
import kotlinx.android.synthetic.main.item_checkout.view.*
import org.eclipse.paho.client.mqttv3.MqttMessage
import org.marproject.reusableadapter.interfaces.AdapterCallback
import org.marproject.reusableadapter.ReusableAdapter

@Suppress("SpellCheckingInspection")
class CheckoutActivity : AppCompatActivity() {

    // buat binding dan viewmodel
    private lateinit var binding: ActivityCheckoutBinding
    private lateinit var viewModel: CheckoutViewModel
    private lateinit var keranjangViewModel: KeranjangViewModel

    // utils
    private val channelId = "checkout"
    private val channelName = "Notification Service"
    private var mqtt: MqttService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // binding & viewmodel init
        binding = ActivityCheckoutBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(this).get(CheckoutViewModel::class.java)

        // keranjang viewmodel init
        val application = requireNotNull(this).application
        val dataSource = KeranjangDatabase.getInstance(application).keranjangDao
        val factory = KeranjangViewModel.Factory(dataSource, application)
        keranjangViewModel = ViewModelProvider(this, factory).get(KeranjangViewModel::class.java)

        // get parcelable data from keranjang
        val dataOrder = intent.getParcelableExtra<OrderProduct>("dataOrder")

        // init mqtt
        mqtt = MqttService(this)

        initUI(dataOrder!!)

        // set content view
        setContentView(binding.root)
    }

    private fun initUI(dataOrderProduct: OrderProduct) {
        // add data to adapter
        keranjangViewModel.keranjang.observe({ lifecycle }, {
            // set adapter
            setupAdapter(binding.rvCheckout, it)
        })

        // set total harga
        binding.tvTotal.text = rupiah(dataOrderProduct.total_price!!.toDouble())

        // action button checkout
        binding.btnCheckout.setOnClickListener {
            DialogUtils.showDialog(this,
                getString(R.string.perhatian),
                getString(R.string.process_disclaimer)) {
                viewModel.processOrder(dataOrderProduct)
            }
        }

        // observe result
        viewModel.result.observe({ lifecycle }, {
            if (it == 1) {
                keranjangViewModel.clearRequest()
                startActivity(Intent(this, HomeActivity::class.java).apply {
                    addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    putExtra("sendNotification", true)
                    putExtra("dataProduct", dataOrderProduct)
                })

                // display notif
                displayNotification(dataOrderProduct)

                // send notif to admin
                val moshi = Moshi.Builder().build()
                val data = moshi.adapter(OrderProduct::class.java).toJson(dataOrderProduct)
                mqtt?.publish("Cheva/BC/Orders/${dataOrderProduct.uid}", MqttMessage(data.toString().toByteArray()))

                Toast.makeText(this, "Berhasil checkout pesanan!", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Gagal checkout pesanan:(", Toast.LENGTH_SHORT).show()
            }
        })

        // back button
        binding.back.setOnClickListener { finish() }
    }

    private fun displayNotification(dataOrder: OrderProduct) {
        val manager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT)
            manager.createNotificationChannel(channel)
        }

        // pending intent
        val intent = Intent(applicationContext, DetailOrderActivity::class.java).apply {
            this.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            this.putExtra("dataOrder", dataOrder)
        }
        val pendingIntent = PendingIntent.getActivity(applicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        // create builder
        val notificationBuilder = NotificationCompat.Builder(applicationContext, channelId)
            .setContentTitle("Berhasil checkout makanan!")
            .setContentText("Silahkan tunggu sampai pesanan anda kami proses\uD83D\uDE0A")
            .setSmallIcon(R.drawable.logo_bakers)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .build()

        manager.notify(2,notificationBuilder)
    }

    private fun setupAdapter(recyclerView: RecyclerView, items: List<Keranjang>) {
        ReusableAdapter<Keranjang>(this)
            .adapterCallback(adapterCallback)
            .setLayout(R.layout.item_checkout)
            .addData(items)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object :
        AdapterCallback<Keranjang> {
        override fun initComponent(itemView: View, data: Keranjang, itemIndex: Int) {
            // set utils
            itemView.tv_nama_makanan.text = data.name
            itemView.tv_qty.text = getString(R.string.show_qty_2, data.qty.toString())

            // set type
            if (data.type == 0) {
                itemView.tv_tipe.text = getString(R.string.show_tipe, "Ready")
            } else {
                itemView.tv_tipe.text = getString(R.string.show_tipe, "Request")
            }

            // set total harga
            itemView.tv_harga_total.text = rupiah(data.price.toDouble() * data.qty.toDouble())
        }

        override fun onItemClicked(itemView: View, data: Keranjang, itemIndex: Int) { }
    }

}
