package com.aitekteam.developer.bakerscorner.ui.search

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscorner.R
import com.aitekteam.developer.bakerscorner.databinding.FragmentDetailCategoryBinding
import com.aitekteam.developer.bakerscorner.model.Product
import com.aitekteam.developer.bakerscorner.model.ProductCategory
import com.aitekteam.developer.bakerscorner.ui.home.HomeViewModel
import com.aitekteam.developer.bakerscorner.ui.home.activity.DetailProdukActivity
import com.aitekteam.developer.bakerscorner.utils.rupiah
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.item_product.view.*
import org.marproject.reusableadapter.ReusableAdapter
import org.marproject.reusableadapter.interfaces.AdapterCallback
import java.lang.StringBuilder

@Suppress("SpellCheckingInspection")
class DetailCategoryFragment : Fragment() {

    // viewmodel & binding
    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentDetailCategoryBinding? = null
    private val binding get() = _binding!!

    // adapter
    private lateinit var productAdapter: ReusableAdapter<Product>

    // utils
    private lateinit var category: ProductCategory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailCategoryBinding.inflate(inflater, container, false)
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

        // cek arguments
        if (arguments != null) {
            category = arguments?.getParcelable("category")!!
            initUI()
        }

        // init adapter
        productAdapter = ReusableAdapter(requireContext())

        // setup product adapter
        setupProductAdapter(binding.rvProduct)

        // Inflate the layout for this fragment
        return binding.root
    }

    private fun initUI() {
        // hide bottom nav
        requireActivity().bottom_nav.visibility = View.GONE

        // change title
        binding.tvCategory.text = category.category

        // fetch products
        homeViewModel.fetchProducts()
        homeViewModel.products.observe(viewLifecycleOwner, Observer {
            // add data into recyclerview
            productAdapter.addData(
                it.filter { p -> p.productCategory?.category == category.category }
                    .sortedByDescending { p -> p.is_available }.toMutableList()
            )
            binding.progressbar.visibility = View.GONE
        })

        // back button
        binding.back.setOnClickListener {
            requireActivity().bottom_nav.visibility = View.VISIBLE
            requireActivity().bottom_nav.menu.getItem(1).isChecked = true
            requireActivity().supportFragmentManager.beginTransaction().replace(R.id.home_frame, SearchFragment()).commit()
        }
    }

    // setup product adapter
    private fun setupProductAdapter(recyclerView: RecyclerView) {
        productAdapter.adapterCallback(productAdapterCallback)
            .setLayout(R.layout.item_product)
            .isGridView(2)
            .build(recyclerView)
    }

    private val productAdapterCallback = object : AdapterCallback<Product> {
        override fun initComponent(itemView: View, data: Product, itemIndex: Int) {
            // set utils
            itemView.tv_nama_produk.text = data.name
            itemView.tv_harga.text = rupiah(data.price!!.toDouble())

            // set gambar product
            Glide.with(requireContext())
                .load(data.photos)
                .into(itemView.image_product)

            // set status makanan
            val setStatus = fun(text: String, color: Int) {
                itemView.tv_status.text = StringBuilder().append(text)
                itemView.status.setBackgroundResource(color)
            }

            when (data.is_available) {
                0 -> setStatus("Habis", R.drawable.status_habis)
                1 -> setStatus("Pre-Order", R.drawable.status_po)
                2 -> setStatus("Ready", R.drawable.status_ready)
            }
        }

        override fun onItemClicked(itemView: View, data: Product, itemIndex: Int) {
            startActivity(
                Intent(requireContext(), DetailProdukActivity::class.java)
                    .putExtra("dataProduct", data)
                    .putExtra("kondisiMasuk", "category")
                    .putExtra("dataKategori", category)
            )
        }
    }

    // back stack
    override fun onResume() {
        super.onResume()
        requireView().isFocusableInTouchMode = true
        requireView().requestFocus()
        requireView().setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                // handle back button's click listener
                requireActivity().bottom_nav.visibility = View.VISIBLE
                requireActivity().bottom_nav.menu.getItem(1).isChecked = true
                requireActivity().supportFragmentManager.beginTransaction().replace(R.id.home_frame, SearchFragment()).commit()
                return@OnKeyListener true
            }
            false
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}