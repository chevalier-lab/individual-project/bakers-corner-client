package com.aitekteam.developer.bakerscorner.model

import android.os.Parcelable
import com.google.firebase.database.Exclude
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class Product (
    var uid: String? = "",
    var name: String? = "",
    var price: Long? = 0,
    var qty: Int? = 0,
    var is_available: Int? = 0,
    var photos: String? = "",
    var search: String? = "",
    var description: String? = "",
    var productCategory: ProductCategory? = ProductCategory(),
    var feedbackItems: MutableList<FeedbackItem>? = mutableListOf(),
    @get:Exclude
    var timestamp: Long? = System.currentTimeMillis()
) : Parcelable {

    override fun equals(other: Any?): Boolean {
        return if (other is Product) {
            other.uid == uid
        } else false
    }

    override fun hashCode(): Int {
        var result = uid?.hashCode() ?: 0
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (price?.hashCode() ?: 0)
        result = 31 * result + (qty ?: 0)
        result = 31 * result + (is_available ?: 0)
        result = 31 * result + (photos?.hashCode() ?: 0)
        result = 31 * result + (description?.hashCode() ?: 0)
        result = 31 * result + (productCategory?.hashCode() ?: 0)
        result = 31 * result + (feedbackItems?.hashCode() ?: 0)
        result = 31 * result + (timestamp?.hashCode() ?: 0)
        return result
    }


}