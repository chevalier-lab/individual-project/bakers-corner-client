package com.aitekteam.developer.bakerscorner.service

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters

class NotificationWorker(
    appContext: Context, params: WorkerParameters
) : CoroutineWorker(appContext, params) {

    private val TAG = "BC-StatusWorker"

    override suspend fun doWork(): Result {
        return try {
            Log.i(TAG, "Worker berjalan")

            // send broadcast
            val broadcastIntent = Intent()
            broadcastIntent.action = "StartService"
            broadcastIntent.setClass(applicationContext, ReceiverService::class.java)
            applicationContext.sendBroadcast(broadcastIntent)

            Log.i(TAG, "Sending broadcast")
            Result.success()
        } catch (exception: Exception) {
            Log.i(TAG, "Memulai ulang worker")
            Result.retry()
        }
    }
}