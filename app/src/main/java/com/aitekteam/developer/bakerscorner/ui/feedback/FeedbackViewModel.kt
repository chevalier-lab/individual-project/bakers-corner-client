package com.aitekteam.developer.bakerscorner.ui.feedback

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aitekteam.developer.bakerscorner.model.*
import com.aitekteam.developer.bakerscorner.utils.Constants
import com.aitekteam.developer.bakerscorner.utils.Constants.NODE_FEEDBACK
import com.aitekteam.developer.bakerscorner.utils.Constants.NODE_FEEDBACK_ITEM
import com.aitekteam.developer.bakerscorner.utils.Constants.NODE_FEEDBACK_ORDER
import com.aitekteam.developer.bakerscorner.utils.Constants.NODE_FEEDBACK_PRODUCT
import com.aitekteam.developer.bakerscorner.utils.Constants.NODE_ORDER
import com.aitekteam.developer.bakerscorner.utils.Constants.NODE_PRODUCT
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

@Suppress("SpellCheckingInspection")
class FeedbackViewModel : ViewModel() {
    // buat database
    private val dbOrder = FirebaseDatabase.getInstance().getReference(NODE_ORDER)
    private val dbProduct = FirebaseDatabase.getInstance().getReference(NODE_PRODUCT)
    private val dbFeedback = FirebaseDatabase.getInstance().getReference(NODE_FEEDBACK)
    private val dbUsers = FirebaseDatabase.getInstance().getReference(Constants.NODE_USERS)

    // buat products
    private val _feedbacks = MutableLiveData<List<FeedbackItem>>()
    val feedbacks: LiveData<List<FeedbackItem>>
        get() = _feedbacks

    // buat product untuk realtime update
    private val _feedback = MutableLiveData<FeedbackItem>()
    val feedback: LiveData<FeedbackItem>
        get() = _feedback

    // buat user
    private val _user = MutableLiveData<User>()
    val user: LiveData<User>
        get() = _user

    // buat loading
    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    // get feedbacks
    fun fetchFeedbacks(productUid: String) {
        dbProduct.child(productUid).child(NODE_FEEDBACK_ITEM).addListenerForSingleValueEvent(valueEventListener)
    }

    private val valueEventListener = object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) { }

        override fun onDataChange(snapshot: DataSnapshot) {
            if (snapshot.exists()) {
                val feedbacks = mutableListOf<FeedbackItem>()

                for (feedbackSnapshot in snapshot.children) {
                    val feedback = feedbackSnapshot.getValue(FeedbackItem::class.java)
                    feedback?.let { feedbacks.add(it) }
                    Log.i("testingFeedback", feedback.toString())
                }

                _feedbacks.value = feedbacks
            } else {
                _feedbacks.value = mutableListOf()
            }
            _loading.value = false
        }
    }

    // get realtime update
    fun getRealtimeUpdate(productUid: String) {
        dbProduct.child(productUid).child(NODE_FEEDBACK_ITEM).addChildEventListener(childEventListener)
    }

    private val childEventListener = object : ChildEventListener {
        override fun onChildChanged(data: DataSnapshot, p1: String?) {
            val feedback = data.getValue(FeedbackItem::class.java)
            _feedback.value = feedback
        }

        override fun onChildAdded(data: DataSnapshot, p1: String?) {
            val feedback = data.getValue(FeedbackItem::class.java)
            _feedback.value = feedback
        }

        override fun onChildRemoved(p0: DataSnapshot) { }

        override fun onCancelled(p0: DatabaseError) { }

        override fun onChildMoved(p0: DataSnapshot, p1: String?) { }

    }

    // add feedback
    fun addFeedback(feedbackProduct: FeedbackProduct, productUid: String, feedbackItem: FeedbackItem, callback: (Task<Void>) -> Unit) {
        feedbackProduct.uid = dbFeedback.child(NODE_FEEDBACK_PRODUCT).push().key
        feedbackItem.uid = dbProduct.push().key

        dbFeedback
            .child(NODE_FEEDBACK_PRODUCT)
            .child(feedbackProduct.uid!!)
            .setValue(feedbackProduct)
            .addOnCompleteListener {
            if (it.isSuccessful) {
                dbProduct
                    .child(productUid)
                    .child("feedbackItems")
                    .child(feedbackItem.uid!!)
                    .setValue(feedbackItem)
                    .addOnCompleteListener { task ->
                        callback(task)
                }
            }
        }
    }

    // add feedback order
    fun addOrderFeedback(feedbackOrder: FeedbackOrder, feedbackItem: FeedbackItem, uidOrder: String, callback: (Task<Void>) -> Unit) {
        feedbackOrder.uid = dbFeedback.child(NODE_FEEDBACK_ORDER).push().key

        dbFeedback
            .child(NODE_FEEDBACK_ORDER)
            .child(feedbackOrder.uid!!)
            .setValue(feedbackOrder)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    dbOrder
                        .child(uidOrder)
                        .child("feedback")
                        .setValue(feedbackItem)
                        .addOnCompleteListener { task ->
                        callback(task)
                    }
                }
            }
    }

    // get user
    fun getUser(uid: String) {
        dbUsers.child(uid).addListenerForSingleValueEvent(initData)
    }

    private val initData = object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) { }

        override fun onDataChange(data: DataSnapshot) {
            if (data.exists()) {
                val key = FirebaseAuth.getInstance()
                val user = data.getValue(User::class.java)
                user!!.uid = key.uid
                _user.value = user
            }
        }
    }

    // findUser
    fun findUser(uid: String, callback: (DataSnapshot) -> Unit) {
        dbUsers.child(uid).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) { }

            override fun onDataChange(snapshot: DataSnapshot) {
                callback(snapshot)
                dbUsers.removeEventListener(this)
            }

        })
    }

    override fun onCleared() {
        super.onCleared()
        dbUsers.removeEventListener(initData)
        dbProduct.removeEventListener(valueEventListener)
        dbProduct.removeEventListener(childEventListener)
    }
}