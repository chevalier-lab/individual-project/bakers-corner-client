package com.aitekteam.developer.bakerscorner.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView

import com.aitekteam.developer.bakerscorner.R
import com.aitekteam.developer.bakerscorner.databinding.FragmentHomeBinding
import com.aitekteam.developer.bakerscorner.model.Product
import com.aitekteam.developer.bakerscorner.model.ProductCategory
import com.aitekteam.developer.bakerscorner.ui.auth.AuthViewModel
import com.aitekteam.developer.bakerscorner.ui.auth.FormUserActivity
import com.aitekteam.developer.bakerscorner.ui.auth.SignInActivity
import com.aitekteam.developer.bakerscorner.ui.feedback.AddFeedbackOrderDialogFragment
import com.aitekteam.developer.bakerscorner.ui.home.activity.DetailProdukActivity
import com.aitekteam.developer.bakerscorner.ui.keranjang.KeranjangActivity
import com.aitekteam.developer.bakerscorner.utils.*
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.item_category.view.*
import kotlinx.android.synthetic.main.item_product.view.*
import org.marproject.reusableadapter.interfaces.AdapterCallback
import org.marproject.reusableadapter.ReusableAdapter
import java.lang.StringBuilder

@Suppress("SpellCheckingInspection")
class HomeFragment : Fragment() {

    // view model & binding
    private lateinit var authViewModel: AuthViewModel
    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    // adapter
    private lateinit var productAdapter: ReusableAdapter<Product>
    private lateinit var categoryAdapter: ReusableAdapter<ProductCategory>

    // utils
    private var rowIndex = -1
    private lateinit var user: FirebaseAuth
    private lateinit var products: MutableList<Product>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        authViewModel = ViewModelProvider(this).get(AuthViewModel::class.java)
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

        // utils init
        products = mutableListOf()
        user = FirebaseAuth.getInstance()

        // init adapter
        productAdapter = ReusableAdapter(requireContext())
        categoryAdapter = ReusableAdapter(requireContext())

        // set progress bar
        binding.progressbar.visibility = View.VISIBLE
        category_shimmer?.visibility = View.VISIBLE
        product_shimmer?.visibility = View.VISIBLE

        // cek user apakah sudah register / belum
        registerCheck()
        loadingCheck()

        // cek apakah order sukses / belum
        ordersCheck()

        // setup product adapter
        setupProductAdapter(binding.rvProduct)

        // init UI
        initUI()

        return binding.root
    }

    private fun initUI() {
        // init category
        homeViewModel.fetchCategories()
        homeViewModel.categories.observe(viewLifecycleOwner, Observer {
            setupCategoryAdapter(binding.rvCategory, it.sortedBy { s -> s.category })
        })

        // init products
        homeViewModel.fetchProducts()
        homeViewModel.products.observe(viewLifecycleOwner, Observer {

            // append data to products for realtime change
//            it.map {  p ->
//                products.add(p)
//            }
            products = it.toMutableList()

            // append data to adapter
            productAdapter.addData(it.sortedByDescending { p -> p.is_available })

            // null data check
            if (it.isEmpty()) {
                binding.tvKosong.visibility = View.VISIBLE
            } else {
                binding.tvKosong.visibility = View.GONE
            }

            // sum of products
            binding.jumlahItem.text = it.size.toString()
        })

        // realtime update for product list
        homeViewModel.realtimeUpdate()
        homeViewModel.product.observe(viewLifecycleOwner, Observer {
//            productAdapter.updateData(it)

            // update products
            if (!products.contains(it)) {
                products.add(it)
            } else {
                val index = products.indexOf(it)
                products[index] = it
            }

            // realtime sort
            productAdapter.addData(
                products.sortedByDescending { p -> p.is_available }
            )
        })

        // show all data
        binding.tvShowAll.setOnClickListener {
            // filter the products
            val filteredProduct = products.sortedByDescending { p -> p.is_available }
            productAdapter.addData(filteredProduct)

            // null data check
            if (filteredProduct.isEmpty()) {
                binding.tvKosong.visibility = View.VISIBLE
            } else {
                binding.tvKosong.visibility = View.GONE
            }

            // unselect the category
            rowIndex = -1
            categoryAdapter.notifyDataSetChanged()

            // jumlah produk
            binding.jumlahItem.text = productAdapter.listData.size.toString()
        }

        // jumlah produk on create activity
        binding.jumlahItem.text = productAdapter.listData.size.toString()
    }

    private fun registerCheck() {
        authViewModel.getUser(user.uid!!)

        authViewModel.user.observe(viewLifecycleOwner, Observer {
            binding.btnKeranjang.setOnClickListener {
                startActivity(
                    Intent(requireContext(), KeranjangActivity::class.java)
                )
            }

            if (it.phone_number == "" && it.address == "") {
                showDialogToFillProfile()
            }

            if (it.userType?.type == 1) {
                showDialogToLogout()
            }
        })
    }

    private fun loadingCheck() {
        authViewModel.loading.observe(viewLifecycleOwner, Observer {
            if (it == false) {
                binding.progressbar.visibility = View.GONE
                category_shimmer?.visibility = View.GONE
                product_shimmer?.visibility = View.GONE
            }
        })
    }
    
    private fun ordersCheck() {
        homeViewModel.checkOrders(user.uid!!)
        homeViewModel.orders.observe(viewLifecycleOwner, Observer {
            it.map { op ->
                if (op.is_accept == 3 && op.feedback?.uid == "") {
                    Log.i("orderCodeSuccess", op.toString())
                    AddFeedbackOrderDialogFragment(op).show(childFragmentManager, "")
                }
            }
        })
    }

    private fun showDialogToFillProfile() {
        DialogUtils.showDialogPositiveOnly(requireContext(),
            getString(R.string.perhatian),
            getString(R.string.lengkapi_profil)) {
            startActivity(
                Intent(requireContext(), FormUserActivity::class.java).apply {
                    this.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                }
            )
        }
    }

    private fun showDialogToLogout() {
        DialogUtils.showDialogPositiveOnly(
            requireContext(),
            getString(R.string.warning),
            getString(R.string.illegal_acces)) {
            logout()
        }
    }

    private fun logout() {
        authViewModel.logout(requireContext(), getString(R.string.default_web_client_id))
        authViewModel.result.observe(viewLifecycleOwner, Observer {
            if (it == 1) {
                val intent = Intent(requireContext(), SignInActivity::class.java).apply {
                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                }
                startActivity(intent)
            }
        })
    }

    // setup category adapter
    private fun setupCategoryAdapter(recyclerView: RecyclerView, items: List<ProductCategory>) {
        categoryAdapter.adapterCallback(categoryAdapterCallback)
            .setLayout(R.layout.item_category)
            .addData(items)
            .isHorizontalView()
            .build(recyclerView)
    }

    // buat object adapterCallback untuk recyclerview
    private val categoryAdapterCallback = object :
        AdapterCallback<ProductCategory> {
        override fun initComponent(itemView: View, data: ProductCategory, itemIndex: Int) {
            // set utils
            itemView.tv_category.text = data.category

            // set icon image
            Glide.with(requireContext())
                .load(data.icon)
                .into(itemView.image_category)

            // set background color on item click
            if (itemIndex == rowIndex) {
                itemView.background_category.setCardBackgroundColor(
                    ContextCompat.getColor(requireContext(), R.color.background_category)
                )
            } else {
                itemView.background_category.setCardBackgroundColor(
                    ContextCompat.getColor(requireContext(), R.color.white)
                )
            }
        }

        override fun onItemClicked(itemView: View, data: ProductCategory, itemIndex: Int) {
            // change product list for each catagory
            val filteredProduct = products.sortedByDescending { p -> p.is_available }
                .filter { p -> p.productCategory!!.category == data.category }

            // update data
            productAdapter.addData(filteredProduct)
            binding.jumlahItem.text = filteredProduct.size.toString()

            if (filteredProduct.isEmpty()) {
                binding.tvKosong.visibility = View.VISIBLE
            } else {
                binding.tvKosong.visibility = View.GONE
            }

            // set index on item clicked
            rowIndex = itemIndex
            categoryAdapter.notifyDataSetChanged()
        }

    }

    // setup product adapter
    private fun setupProductAdapter(recyclerView: RecyclerView) {
        productAdapter.adapterCallback(productAdapterCallback)
            .setLayout(R.layout.item_product)
            .isGridView(2)
            .build(recyclerView)
    }

    private val productAdapterCallback = object : AdapterCallback<Product> {
        override fun initComponent(itemView: View, data: Product, itemIndex: Int) {
            // set utils
            itemView.tv_nama_produk.text = data.name
            itemView.tv_harga.text = rupiah(data.price!!.toDouble())

            // set gambar product
            Glide.with(requireContext())
                .load(data.photos)
                .into(itemView.image_product)

            // set status makanan
            val setStatus = fun(text: String, color: Int) {
                itemView.tv_status.text = StringBuilder().append(text)
                itemView.status.setBackgroundResource(color)
            }

            when (data.is_available) {
                0 -> setStatus("Habis", R.drawable.status_habis)
                1 -> setStatus("Pre-Order", R.drawable.status_po)
                2 -> setStatus("Ready", R.drawable.status_ready)
            }
        }

        override fun onItemClicked(itemView: View, data: Product, itemIndex: Int) {
            startActivity(
                Intent(requireContext(), DetailProdukActivity::class.java)
                    .putExtra("dataProduct", data)
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
