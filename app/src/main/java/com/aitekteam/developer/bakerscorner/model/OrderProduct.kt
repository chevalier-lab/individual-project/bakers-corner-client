package com.aitekteam.developer.bakerscorner.model

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class OrderProduct (
    var uid: String? = "",
    var time_stamp: Long? = System.currentTimeMillis(),
    var total_price: Long? = 0,
    var total_qty: Int? = 0,
    var is_accept: Int? = 0,
    var order_code: String? = "",
    var type: MutableList<Int>? = mutableListOf(),
    var user: User? = User(),
    var products: MutableList<Product>? = arrayListOf(),
    var productRequests: MutableList<ProductRequest>? = arrayListOf(),
    var feedback: FeedbackItem? = FeedbackItem()
) : Parcelable {

    override fun equals(other: Any?): Boolean {
        return if (other is OrderProduct) {
            other.uid == uid
        } else false
    }

    override fun hashCode(): Int {
        var result = uid?.hashCode() ?: 0
        result = 31 * result + (time_stamp?.hashCode() ?: 0)
        result = 31 * result + (total_price?.hashCode() ?: 0)
        result = 31 * result + (total_qty ?: 0)
        result = 31 * result + (is_accept ?: 0)
        result = 31 * result + (order_code?.hashCode() ?: 0)
        result = 31 * result + (type?.hashCode() ?: 0)
        result = 31 * result + (user?.hashCode() ?: 0)
        result = 31 * result + (products?.hashCode() ?: 0)
        result = 31 * result + (productRequests?.hashCode() ?: 0)
        result = 31 * result + (feedback?.hashCode() ?: 0)
        return result
    }

}

