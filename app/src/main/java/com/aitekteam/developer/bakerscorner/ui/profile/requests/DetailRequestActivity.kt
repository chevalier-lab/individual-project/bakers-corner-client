package com.aitekteam.developer.bakerscorner.ui.profile.requests

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.aitekteam.developer.bakerscorner.R
import com.aitekteam.developer.bakerscorner.databinding.ActivityDetailRequestBinding
import com.aitekteam.developer.bakerscorner.model.OrderProduct
import com.aitekteam.developer.bakerscorner.model.ProductCategory
import com.aitekteam.developer.bakerscorner.model.ProductRequest
import com.aitekteam.developer.bakerscorner.ui.home.HomeViewModel
import com.aitekteam.developer.bakerscorner.ui.request.RequestViewModel
import com.aitekteam.developer.bakerscorner.utils.DialogUtils
import com.aitekteam.developer.bakerscorner.utils.DropdownAdapter
import com.aitekteam.developer.bakerscorner.utils.SnackbarUtils
import com.google.firebase.auth.FirebaseAuth

@Suppress("SpellCheckingInspection")
class DetailRequestActivity : AppCompatActivity() {

    // viewmodel & binding
    private lateinit var viewModel: RequestViewModel
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var binding: ActivityDetailRequestBinding

    // utils
    private lateinit var user: FirebaseAuth
    private lateinit var orderProduct: OrderProduct
    private lateinit var productRequest: ProductRequest
    private lateinit var listCategory: ArrayList<String>
    private lateinit var listCategoryObj: MutableList<ProductCategory>

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // init binding & viewmodel
        binding = ActivityDetailRequestBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(this).get(RequestViewModel::class.java)
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

        // utils init
        user = FirebaseAuth.getInstance()
        listCategory = arrayListOf()
        listCategoryObj = mutableListOf()
        orderProduct = OrderProduct()
        productRequest = ProductRequest()

        // fetch category
        homeViewModel.fetchCategories()

        // get data request from orders
        viewModel.getOrders { }

        initUI()

        // set content view
        setContentView(binding.root)
    }

    private fun initUI() {
        // get data
        initData(intent.getParcelableExtra("request")!!)

        // dropdownlist
        DropdownAdapter(this, binding.category, listCategory) {
            // set data of category
            homeViewModel.categories.observe({ lifecycle }, { category ->
                category.map { p ->
                    listCategory.add(p.category.toString())
                    it.notifyDataSetChanged()
                    listCategoryObj.add(p)
                }
            })
        }.show()

        // cancel request
        binding.btnCancel.setOnClickListener {
            DialogUtils.showDialog(this,
                getString(R.string.warning),
                getString(R.string.cancel_request_disclaimer)) {
                doCancel(it)
            }
        }

        // edit request
        binding.btnUpdate.setOnClickListener { inputCheck(it) }

        // back
        binding.btnBack.setOnClickListener { finish() }
    }

    private fun initData(data: ProductRequest) {
        // get data from firebase
        viewModel.orders.observe({ lifecycle }, {
            it.map {  op ->
                if (op.productRequests!!.contains(data)) {
                    orderProduct = op
                }
            }
        })

        // get productRequest data
        productRequest = data

        // inject data to view
        binding.category.setText(data.productCategory!!.category)
        binding.etNamaMakanan.setText(data.name)
        binding.etKeterangan.setText(data.description)
        binding.etHarga.setText(data.price.toString())
        binding.etQty.setText(data.qty.toString())
    }

    private fun inputCheck(view: View) {
        when {
            binding.etNamaMakanan.text!!.trim().isEmpty() -> {
                binding.layoutName.error ="Harap isi nama makanan!"
            }
            binding.etHarga.text!!.trim().isEmpty() -> {
                binding.layoutHarga.error ="Harap isi kisaran harga!"
            }
            binding.etQty.text!!.trim().isEmpty() -> {
                binding.layoutQty.error ="Harap isi kuantitas makanan!"
            }
            binding.etKeterangan.text!!.trim().isEmpty() -> {
                binding.layoutKeterangan.error ="Harap isi keterangan makanan!"
            }
            else -> {
                DialogUtils.showDialog(this,
                    getString(R.string.perhatian),
                    getString(R.string.update_req_disclaimer)
                ) { doUpdate(view) }
            }
        }
    }

    private fun doUpdate(view: View) {
        val name = binding.etNamaMakanan.text.toString()
        val description = binding.etKeterangan.text.toString()
        val price = binding.etHarga.text.toString().toLong()
        val qty = binding.etQty.text.toString().toInt()
        val category = binding.category.text.toString()

        // get category object
        val objCategory = ProductCategory()
        listCategoryObj.map {
            if (it.category == category) {
                objCategory.uid = it.uid
                objCategory.icon = it.icon
                objCategory.category = category
            }
        }

        // harga total sebelum update
        val hargaTotal = orderProduct.total_price
        val hargaRequest = productRequest.price!! * productRequest.qty!!.toLong()
        val hargaTotalKurang = hargaTotal!!.minus(hargaRequest)

        // harga total setalah update
        val hargaRequestUpdate = price * qty.toLong()
        val hargaTotalUpdate = hargaTotalKurang.plus(hargaRequestUpdate)

        // qty sebelum update
        val totalQty = orderProduct.total_qty!!
        val totalQtyKurang = totalQty.minus(productRequest.qty!!)

        // qty setelah update
        val totalQtyUpdate = totalQtyKurang + qty

        // create data
        val dataRequest = ProductRequest(productRequest.uid, name, price,
            qty, 0, 0, description, objCategory, System.currentTimeMillis())

        // append another data to orderProduct
        val index = orderProduct.productRequests!!.indexOf(dataRequest)
        orderProduct.productRequests!![index] = dataRequest
        orderProduct.total_price = hargaTotalUpdate
        orderProduct.total_qty = totalQtyUpdate

        // run updateData
        viewModel.updateData(orderProduct) {
            if (it.isSuccessful) {
//                getSnackbar(view, "Berhasil update request!")
                startActivity(
                    Intent(this, ListRequestActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .putExtra("notifSuccess", true)
                )
            } else {
                getSnackbar(view, "Gagal update request:(")
            }
        }

        // update productRequest object
        productRequest = dataRequest
    }

    private fun doCancel(view: View) {
        if (orderProduct.products!!.isEmpty() && orderProduct.productRequests!!.size == 1) {
            viewModel.deleteDataOrder(orderProduct) {
                if (it.isSuccessful) {
                    startActivity(
                        Intent(this, ListRequestActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .putExtra("notifCancel", true)
                    )
                } else {
                    getSnackbar(view, "Gagal hapus request:(")
                }
            }
        } else if (orderProduct.products!!.isNotEmpty() && orderProduct.productRequests!!.size == 1) {
            // update qty & price on orders
            val totalHargaRequest = productRequest.price!! * productRequest.qty!!.toLong()
            val totalHargaOrder = orderProduct.total_price!!.minus(totalHargaRequest)
            val totalQtyOrder = orderProduct.total_qty!!.minus(productRequest.qty!!)

            // update object orderProduct
            orderProduct.total_price = totalHargaOrder
            orderProduct.total_qty = totalQtyOrder

            // update type of orderProduct
            orderProduct.type!!.remove(1)

            // update orders
            viewModel.updateData(orderProduct) {}

            // delete request
            viewModel.deleteRequestConditional(orderProduct.uid!!) {
                if (it.isSuccessful) {
                    startActivity(
                        Intent(this, ListRequestActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .putExtra("notifCancel", true)
                    )
                } else {
                    getSnackbar(view, "Gagal hapus request:(")
                }
            }
        } else {
            // remove item from list
            orderProduct.productRequests!!.remove(productRequest)

            // update qty & price on orders
            val totalHargaRequest = productRequest.price!! * productRequest.qty!!.toLong()
            val totalHargaOrder = orderProduct.total_price!!.minus(totalHargaRequest)
            val totalQtyOrder = orderProduct.total_qty!!.minus(productRequest.qty!!)

            // update object orderProduct
            orderProduct.total_price = totalHargaOrder
            orderProduct.total_qty = totalQtyOrder

            // update orders
            viewModel.updateData(orderProduct) {
                if (it.isSuccessful) {
                    startActivity(
                        Intent(this, ListRequestActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .putExtra("notifCancel", true)
                    )
                } else {
                    getSnackbar(view, "Gagal hapus request:(")
                }
            }
        }
    }

    private fun getSnackbar(view: View, message: String) {
        SnackbarUtils.show(view, message,
            ContextCompat.getColor(this, R.color.colorPrimary),
            ContextCompat.getColor(this, R.color.text_hitam_utama),
            ContextCompat.getColor(this, R.color.white))
    }

}
