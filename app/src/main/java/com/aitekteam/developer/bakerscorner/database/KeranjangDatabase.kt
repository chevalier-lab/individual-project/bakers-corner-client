package com.aitekteam.developer.bakerscorner.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Suppress("SpellCheckingInspection")
@Database(entities = [Keranjang::class], version = 1, exportSchema = false)
abstract class KeranjangDatabase : RoomDatabase() {

    abstract val keranjangDao: KeranjangDao

    companion object {
        @Volatile
        private var INSTANCE: KeranjangDatabase? = null

        fun getInstance(context: Context) : KeranjangDatabase {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        KeranjangDatabase::class.java,
                        "keranjang_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}