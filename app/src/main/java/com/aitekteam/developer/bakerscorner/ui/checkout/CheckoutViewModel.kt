package com.aitekteam.developer.bakerscorner.ui.checkout

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aitekteam.developer.bakerscorner.model.OrderProduct
import com.aitekteam.developer.bakerscorner.utils.Constants.NODE_ORDER
import com.google.firebase.database.FirebaseDatabase
import java.util.*

@Suppress("SpellCheckingInspection")
class CheckoutViewModel : ViewModel() {

    // buat database
    private val dbOrder = FirebaseDatabase.getInstance().getReference(NODE_ORDER)

    // buat result
    private val _result = MutableLiveData<Int>()
    val result: LiveData<Int>
        get() = _result

    // fungsi insert order
    fun processOrder(data: OrderProduct) {
        data.uid = dbOrder.push().key
        data.order_code = UUID.randomUUID().toString()
            .split("-")[4].toUpperCase(Locale.ROOT)

        dbOrder.child(data.uid!!).setValue(data).addOnCompleteListener {
            if (it.isSuccessful) {
                _result.value = 1
            } else {
                _result.value = 0
            }
        }
    }

}