package com.aitekteam.developer.bakerscorner.model

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class User (
    var uid: String? = "",
    var email: String? = "",
    var name: String? = "",
    var phone_number: String? = "",
    var address: String? = "",
    var face: String? = "",
    var userType: UserType? = UserType()
) : Parcelable
