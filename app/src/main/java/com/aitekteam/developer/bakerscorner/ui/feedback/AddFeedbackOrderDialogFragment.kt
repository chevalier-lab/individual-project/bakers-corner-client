package com.aitekteam.developer.bakerscorner.ui.feedback

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.aitekteam.developer.bakerscorner.databinding.DialogFragmentAddFeedbackOrderBinding
import com.aitekteam.developer.bakerscorner.model.FeedbackItem
import com.aitekteam.developer.bakerscorner.model.FeedbackOrder
import com.aitekteam.developer.bakerscorner.model.OrderProduct
import java.lang.StringBuilder
import java.util.*

class AddFeedbackOrderDialogFragment(
    private val dataOrder: OrderProduct
) : DialogFragment() {

    // binding & viewmodel
    private var _binding: DialogFragmentAddFeedbackOrderBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: FeedbackViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DialogFragmentAddFeedbackOrderBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this).get(FeedbackViewModel::class.java)
        binding.tvOrder.text = StringBuilder().append("Order #").append(dataOrder.order_code)
        isCancelable = false
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, android.R.style.Theme_Material_Light_Dialog_MinWidth)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.btnAddFeedback.setOnClickListener {
            val feedback = binding.etFeedback.text.toString().trim()

            if (feedback.isEmpty()) {
                binding.layoutName.error = "Harap isi feedback!"
                return@setOnClickListener
            }

            // add to feedbackOrder
            val feedbackOrder = FeedbackOrder("", dataOrder, feedback)

            // create feedbackItem
            val uidFeedback = UUID.randomUUID().toString()
                .split("-")[4].toUpperCase(Locale.ROOT)
            val feedbackItem = FeedbackItem(uidFeedback, dataOrder.user, feedback)

            viewModel.addOrderFeedback(feedbackOrder, feedbackItem, dataOrder.uid!!) {
                if (it.isSuccessful) {
                    dismiss()
                } else {
                    dismiss()
                }
            }
        }
    }
}