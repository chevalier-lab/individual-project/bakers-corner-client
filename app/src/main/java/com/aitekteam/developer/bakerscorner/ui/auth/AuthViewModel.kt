package com.aitekteam.developer.bakerscorner.ui.auth

import android.content.Context
import androidx.lifecycle.*
import com.aitekteam.developer.bakerscorner.model.User
import com.aitekteam.developer.bakerscorner.utils.Constants.NODE_USERS
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

@Suppress("SpellCheckingInspection")
class AuthViewModel : ViewModel() {

    // buat current user
    private lateinit var mGoogleSignInClient: GoogleSignInClient

    // buat database
    private val dbUsers = FirebaseDatabase.getInstance().getReference(NODE_USERS)

    // buat user
    private val _user = MutableLiveData<User>()
    val user: LiveData<User>
        get() = _user

    // buat result
    private val _result = MutableLiveData<Int>()
    val result: LiveData<Int>
        get() = _result

    // buat loading
    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    init {
        _loading.value = true
    }

    // fungsi untuk update profile
    fun updateProfile(user: User) {
        dbUsers.child(user.uid!!).setValue(user).addOnCompleteListener {
            if (it.isSuccessful) {
                _result.value = 1
            } else {
                _result.value = 0
            }
        }
    }

    // fungsi untuk cek user
    fun userCheck(uid: String) {
        dbUsers.child(uid).addListenerForSingleValueEvent(userCheck)
    }

    private val userCheck = object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) { }

        override fun onDataChange(data: DataSnapshot) {
            // jika belum pernah daftar
            if (!data.exists()) {
                val key = FirebaseAuth.getInstance()
                val user = key.currentUser
                val dataKirim = User(key.uid, user!!.displayName, user.email)
                dbUsers.child(key.uid!!).setValue(dataKirim).addOnCompleteListener {
                    if (it.isSuccessful) {
                        _result.value = 1
                    } else {
                        _result.value = 0
                    }
                }
            } else {
                _result.value = 2
            }
        }

    }

    // fungsi cek sudah register / belum
    fun getUser(uid: String) {
        dbUsers.child(uid).addListenerForSingleValueEvent(getUser)
    }

    private val getUser = object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) { }

        override fun onDataChange(data: DataSnapshot) {
            if (data.exists()) {
                val key = FirebaseAuth.getInstance()
                val user = data.getValue(User::class.java)
                user!!.uid = key.uid
                _user.value = user
                _loading.value = false
            }
        }

    }

    // fungsi logout
    fun logout(context: Context, webClientId: String) {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(webClientId)
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(context, gso)
        FirebaseAuth.getInstance().signOut()
        mGoogleSignInClient.signOut()

        _result.value = 1
    }

    override fun onCleared() {
        super.onCleared()
        dbUsers.removeEventListener(getUser)
    }

}