package com.aitekteam.developer.bakerscorner.ui.search

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.RecyclerView

import com.aitekteam.developer.bakerscorner.R
import com.aitekteam.developer.bakerscorner.databinding.FragmentSearchBinding
import com.aitekteam.developer.bakerscorner.model.Product
import com.aitekteam.developer.bakerscorner.model.ProductCategory
import com.aitekteam.developer.bakerscorner.ui.home.HomeFragment
import com.aitekteam.developer.bakerscorner.ui.home.activity.DetailProdukActivity
import com.aitekteam.developer.bakerscorner.utils.Constants.NODE_CATEGORY
import com.aitekteam.developer.bakerscorner.utils.Constants.NODE_PRODUCT
import com.aitekteam.developer.bakerscorner.utils.rupiah
import com.bumptech.glide.Glide
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.grid_category.view.*
import kotlinx.android.synthetic.main.item_product.view.*
import org.marproject.reusableadapter.ReusableAdapter
import org.marproject.reusableadapter.interfaces.AdapterCallback
import java.lang.StringBuilder
import java.util.*

@Suppress("SpellCheckingInspection")
class SearchFragment : Fragment() {

    // viewmodel & binding
    private var _binding: FragmentSearchBinding? = null
    private val binding get() = _binding!!

    // utils
    private lateinit var products: MutableList<Product>
    private lateinit var categories: MutableList<ProductCategory>
    private lateinit var listColor: MutableList<String>
    private lateinit var adapter: ReusableAdapter<Product>
    private lateinit var categoryAdapter: ReusableAdapter<ProductCategory>
    private lateinit var firebaseDatabase: DatabaseReference

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSearchBinding.inflate(inflater, container, false)

        // init products
        products = mutableListOf()
        categories = mutableListOf()

        // init adapter
        adapter = ReusableAdapter(requireContext())
        categoryAdapter = ReusableAdapter(requireContext())

        // setup adapter
        setupAdapter.product(binding.rvProduct)
        setupAdapter.category(binding.rvCategory)

        //init db
        firebaseDatabase = FirebaseDatabase.getInstance().reference

        // init category
        initCategory()

        // add color
        listColor = mutableListOf(
            "#b0e2d0", "#ebc8c5", "#fff1a1", "#c7dea2", "#baaabd", "#ffe4b1",
            "#EF9A9A", "#F48FB1", "#CE93D8", "#90CAF9", "#81D4FA", "#A5D6A7"
        )

        // search box
        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) { }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0.toString() == "") {
                    products.clear()
                    adapter.addData(products)
                    binding.rvCategory.visibility = View.VISIBLE
                    binding.rvProduct.visibility = View.GONE
                } else {
                    binding.rvCategory.visibility = View.GONE
                    binding.rvProduct.visibility = View.VISIBLE
                    searhProducts(p0.toString().toLowerCase(Locale.ROOT))
                }
            }

        })

        // Inflate the layout for this fragment
        return binding.root
    }

    // init list category
    private fun initCategory() {
        firebaseDatabase.child(NODE_CATEGORY).addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) { }

            override fun onDataChange(snapshot: DataSnapshot) {
                for (categorySnapshot in snapshot.children) {
                    val category = categorySnapshot.getValue(ProductCategory::class.java)
                    category?.let { categories.add(it) }
                }

                binding.rvProduct.visibility = View.GONE
                categoryAdapter.addData(categories)
                firebaseDatabase.child(NODE_CATEGORY).removeEventListener(this)
            }

        })
    }

    // search product
    private fun searhProducts(textSearch: String) {
        val query = firebaseDatabase.child(NODE_PRODUCT).orderByChild("search").startAt(textSearch).endAt(textSearch + "\uf8ff")

        query.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) { }

            override fun onDataChange(snapshot: DataSnapshot) {
                products.clear()
                for (productSnapshot in snapshot.children) {
                    val product = Product(
                        productSnapshot.child("uid").getValue(String::class.java),
                        productSnapshot.child("name").getValue(String::class.java),
                        productSnapshot.child("price").getValue(Long::class.java),
                        productSnapshot.child("qty").getValue(Int::class.java),
                        productSnapshot.child("_available").getValue(Int::class.java),
                        productSnapshot.child("photos").getValue(String::class.java),
                        productSnapshot.child("search").getValue(String::class.java),
                        productSnapshot.child("description").getValue(String::class.java),
                        productSnapshot.child("productCategory").getValue(ProductCategory::class.java)
                    )
                    products.add(product)
                }
                adapter.addData(products)

                // remove listener
                firebaseDatabase.child(NODE_PRODUCT).removeEventListener(this)
            }

        })
    }

    // setup adapter
    private val setupAdapter = object {
        fun product(recyclerView: RecyclerView) {
            adapter.adapterCallback(adapterCallback.product)
                .setLayout(R.layout.item_product)
                .isGridView(2)
                .build(recyclerView)
        }

        fun category(recyclerView: RecyclerView) {
            categoryAdapter.adapterCallback(adapterCallback.category)
                .setLayout(R.layout.grid_category)
                .isGridView(2)
                .build(recyclerView)
        }
    }

    // adapter callback
    private val adapterCallback = object {
        val product = object : AdapterCallback<Product> {
            override fun initComponent(itemView: View, data: Product, itemIndex: Int) {
                // set utils
                itemView.tv_nama_produk.text = data.name
                itemView.tv_harga.text = rupiah(data.price!!.toDouble())

                // set gambar product
                Glide.with(requireContext())
                    .load(data.photos)
                    .into(itemView.image_product)

                // set status makanan
                val setStatus = fun(text: String, color: Int) {
                    itemView.tv_status.text = StringBuilder().append(text)
                    itemView.status.setBackgroundResource(color)
                }

                when (data.is_available) {
                    0 -> setStatus("Habis", R.drawable.status_habis)
                    1 -> setStatus("Pre-Order", R.drawable.status_po)
                    2 -> setStatus("Ready", R.drawable.status_ready)
                }
            }

            override fun onItemClicked(itemView: View, data: Product, itemIndex: Int) {
                startActivity(
                    Intent(requireContext(), DetailProdukActivity::class.java)
                        .putExtra("dataProduct", data)
                        .putExtra("kondisiMasuk", "search")
                )
            }
        }

        val category = object : AdapterCallback<ProductCategory> {
            override fun initComponent(itemView: View, data: ProductCategory, itemIndex: Int) {
                itemView.tv_category.text = data.category

                // onclick layout
                itemView.layout_category.setOnClickListener {
                    val bundle = bundleOf("category" to data)
                    val fragment = DetailCategoryFragment().apply {
                        this.arguments = bundle
                    }
                    requireActivity().supportFragmentManager.beginTransaction().replace(R.id.home_frame, fragment).commit()
                }

                // get random color
                val color = listColor.random().toString()
                itemView.layout_category.setCardBackgroundColor(Color.parseColor(color))

                // remove temp color
                listColor.remove(color)
            }

            override fun onItemClicked(itemView: View, data: ProductCategory, itemIndex: Int) { }
        }
    }

    // back stack
    override fun onResume() {
        super.onResume()
        requireView().isFocusableInTouchMode = true
        requireView().requestFocus()
        requireView().setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                // handle back button's click listener
                requireActivity().bottom_nav.menu.getItem(0).isChecked = true
                requireActivity().supportFragmentManager.beginTransaction().replace(R.id.home_frame, HomeFragment()).commit()
                return@OnKeyListener true
            }
            false
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}
