package com.aitekteam.developer.bakerscorner.ui.home.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.aitekteam.developer.bakerscorner.R
import com.aitekteam.developer.bakerscorner.database.Keranjang
import com.aitekteam.developer.bakerscorner.database.KeranjangDatabase
import com.aitekteam.developer.bakerscorner.databinding.ActivityDetailProdukBinding
import com.aitekteam.developer.bakerscorner.model.Product
import com.aitekteam.developer.bakerscorner.model.ProductCategory
import com.aitekteam.developer.bakerscorner.ui.feedback.FeedbackActivity
import com.aitekteam.developer.bakerscorner.ui.feedback.FeedbackViewModel
import com.aitekteam.developer.bakerscorner.ui.keranjang.KeranjangActivity
import com.aitekteam.developer.bakerscorner.ui.keranjang.KeranjangViewModel
import com.aitekteam.developer.bakerscorner.utils.DialogUtils
import com.aitekteam.developer.bakerscorner.utils.SnackbarUtils
import com.aitekteam.developer.bakerscorner.utils.rupiah
import com.bumptech.glide.Glide

@Suppress("SpellCheckingInspection")
class DetailProdukActivity : AppCompatActivity() {

    // binding
    private lateinit var binding: ActivityDetailProdukBinding

    // viewmodel
    private lateinit var viewModel: FeedbackViewModel
    private lateinit var keranjangViewModel: KeranjangViewModel

    // utils
    private var qty = 1
    private var oldQty = 0
    private var dataExists = false
    private var totalPrice: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailProdukBinding.inflate(layoutInflater)

        // keranjang view model init
        val application = requireNotNull(this).application
        val dataSource = KeranjangDatabase.getInstance(application).keranjangDao
        val factory = KeranjangViewModel.Factory(dataSource, application)
        keranjangViewModel = ViewModelProvider(this, factory).get(KeranjangViewModel::class.java)

        // feedback viewmodel init
        viewModel = ViewModelProvider(this).get(FeedbackViewModel::class.java)

        // get parcelable data
        val dataProduct = intent.getParcelableExtra<Product>("dataProduct")!!

        // init data on room db for checking data if already exists on cart
        keranjangViewModel.keranjang.observe({ lifecycle }, {
            it.map { k ->
                if (k.name == dataProduct.name) {
                    oldQty = k.qty
                    dataExists = true
                }
            }
        })

        // init UI
        initUI(dataProduct)

        // set content view
        setContentView(binding.root)
    }

    private fun initUI(dataProduct: Product) {
        // set image of product
        Glide.with(this)
            .load(dataProduct.photos)
            .into(binding.imageProduct)

        viewModel.fetchFeedbacks(dataProduct.uid!!)
        viewModel.feedbacks.observe({ lifecycle }, {
            binding.tvFeedback.text = StringBuilder().append("Total ulasan : ").append(it.size)
        })

        // set description
        binding.tvNamaMakanan.text = dataProduct.name
        binding.tvHarga.text = rupiah(dataProduct.price!!.toDouble())
        binding.tvDeskripsi.text = dataProduct.description
        binding.tvCategory.text = dataProduct.productCategory!!.category

        // fun set status
        val setStatus = fun(stringRes: Int, color: Int) {
            binding.tvStatus.text = getString(stringRes)
            binding.tvStatus.setTextColor(resources.getColor(color))

        }

        // set status
        when (dataProduct.is_available) {
            0 -> setStatus(R.string.habis, R.color.text_red)
            1 -> setStatus(R.string.pre_order, R.color.text_orange)
            else -> setStatus(R.string.ready, R.color.text_green)
        }

        // cek makanan ready / tidak
        if (dataProduct.is_available == 0) {
            binding.btnQty.visibility = View.GONE
        }

        // back button
        binding.btnBack.setOnClickListener { finish() }

        // cart button
        binding.btnKeranjang.setOnClickListener {
            startActivity(
                Intent(this, KeranjangActivity::class.java)
            )
        }

        // feedback button
        binding.btnFeedback.setOnClickListener {
            startActivity(
                Intent(this, FeedbackActivity::class.java)
                    .putExtra("dataProduk", dataProduct)
            )
        }

        // buy button
        binding.btnPesan.setOnClickListener {
            if (dataProduct.is_available != 0) {
                showDialogToProcess(dataProduct)
            } else {
                SnackbarUtils.withoutAction(window.decorView, "Maaf makanan ini habis:(",
                    ContextCompat.getColor(this, R.color.text_hitam_utama),
                    ContextCompat.getColor(this, R.color.white)
                )
            }
        }

        // item count
        itemCount(dataProduct)
    }

    private fun itemCount(dataProduct: Product) {
        binding.tvMin.setOnClickListener {
            qty = qty.minus(1)
            if (qty == 0) qty = 1
            if (qty >= 1) binding.tvQty.text = qty.toString()

            val price = dataProduct.price
            totalPrice = price?.times(qty)!!

            binding.tvHarga.text = rupiah(totalPrice.toDouble())
        }

        binding.tvPlus.setOnClickListener {
            qty = qty.plus(1)
            binding.tvQty.text = qty.toString()

            val price = dataProduct.price
            totalPrice = price?.times(qty)!!
            binding.tvHarga.text = rupiah(totalPrice.toDouble())
        }
    }

    private fun doProcess(dataProduct: Product) {
        // init data
        val data = Keranjang()
        data.name = dataProduct.name!!
        data.price = dataProduct.price!!
        data.qty = qty
        data.is_available = dataProduct.is_available!!
        data.photos = dataProduct.photos!!
        data.description = dataProduct.description!!
        data.categoryId = dataProduct.productCategory?.uid!!
        data.productCategory = dataProduct.productCategory?.category!!
        data.icon = dataProduct.productCategory?.icon!!
        data.productUid = dataProduct.uid!!

        // check if product already added to cart before
        if (dataExists) { // update product on cart
            data.qty += oldQty
            keranjangViewModel.updateKeranjang(data.qty, data.name)
        } else { // add to local db
            keranjangViewModel.inserDataKeranjang(data)
        }

        // move to keranjang activity
        startActivity(
            Intent(this, KeranjangActivity::class.java)
        )
    }

    private fun showDialogToProcess(dataProduct: Product) {
        DialogUtils.showDialog(this,
            getString(R.string.perhatian),
            getString(R.string.pesan_disclaimer)) {
            doProcess(dataProduct)
        }
    }

}
