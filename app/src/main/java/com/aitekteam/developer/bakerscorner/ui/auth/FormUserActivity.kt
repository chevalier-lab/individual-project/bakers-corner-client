package com.aitekteam.developer.bakerscorner.ui.auth

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.aitekteam.developer.bakerscorner.R
import com.aitekteam.developer.bakerscorner.databinding.ActivityFormUserBinding
import com.aitekteam.developer.bakerscorner.model.User
import com.aitekteam.developer.bakerscorner.model.UserType
import com.aitekteam.developer.bakerscorner.ui.home.activity.HomeActivity
import com.aitekteam.developer.bakerscorner.utils.DialogUtils
import com.google.firebase.auth.FirebaseAuth

@Suppress("SpellCheckingInspection")
class FormUserActivity : AppCompatActivity() {

    private lateinit var binding: ActivityFormUserBinding
    private lateinit var viewModel: AuthViewModel
    private lateinit var user: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFormUserBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(this).get(AuthViewModel::class.java)
        user = FirebaseAuth.getInstance()

        setupUI()

        // set content view
        setContentView(binding.root)
    }

    private fun setupUI() {
        val user = FirebaseAuth.getInstance().currentUser

        user?.let {
            val name = user.displayName
            val email = user.email

            binding.etFullname.setText(name)
            binding.etEmail.setText(email)
        }

        binding.btnSaveData.setOnClickListener {
            inputCheck()
        }

        viewModel.result.observe({ lifecycle }) {
            if (it == 1) {
                Toast.makeText(this, "Berhasil update profil!", Toast.LENGTH_SHORT).show()
                startActivity(
                    getLaunchIntent(
                        this
                    )
                )
            } else {
                Toast.makeText(this, "Gagal nambah data!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun inputCheck() {
        when {
            binding.etFullname.text!!.trim().isEmpty() -> {
                binding.layoutName.error = "Nama wajib diisi!"
            }
            binding.etEmail.text!!.trim().isEmpty() -> {
                binding.layoutEmail.error = "Email wajib diisi!"
            }
            binding.etNoHp.text!!.trim().isEmpty() -> {
                binding.layoutPhone.error = "Nomor HP wajib diisi!"
            }
            binding.etAlamat.text!!.trim().isEmpty() -> {
                binding.layoutAddress.error = "Alamat wajib diisi!"
            }
            else -> showDialogToProcess()
        }
    }

    private fun showDialogToProcess() {
        DialogUtils.showDialog(this,
            getString(R.string.perhatian),
            getString(R.string.register_disclaimer)) {
            saveData()
        }
    }

    private fun saveData() {
        val nama = binding.etFullname.text.toString()
        val email = binding.etEmail.text.toString()
        val phone = binding.etNoHp.text.toString()
        val alamat = binding.etAlamat.text.toString()

        val data = User(user.uid, email, nama, phone, alamat,
            user.currentUser!!.photoUrl.toString(), UserType(user.uid))
        viewModel.updateProfile(data)
    }

    companion object {
        fun getLaunchIntent(from: Context) = Intent(from, HomeActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
    }

}
